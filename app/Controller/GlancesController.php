<?php
App::uses('AppController', 'Controller');
/**
 * Glances Controller
 *
 * @property Glance $Glance
 * @property PaginatorComponent $Paginator
 */
class GlancesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Glance->recursive = 0;
		$this->set('glances', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Glance->exists($id)) {
			throw new NotFoundException(__('Invalid glance'));
		}
		$options = array('conditions' => array('Glance.' . $this->Glance->primaryKey => $id));
		$this->set('glance', $this->Glance->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Glance->create();
			if ($this->Glance->save($this->request->data)) {
				$this->Flash->success(__('The glance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The glance could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Glance->exists($id)) {
			throw new NotFoundException(__('Invalid glance'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Glance->save($this->request->data)) {
				$this->Flash->success(__('The glance has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The glance could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Glance.' . $this->Glance->primaryKey => $id));
			$this->request->data = $this->Glance->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Glance->exists($id)) {
			throw new NotFoundException(__('Invalid glance'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Glance->delete($id)) {
			$this->Flash->success(__('The glance has been deleted.'));
		} else {
			$this->Flash->error(__('The glance could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
