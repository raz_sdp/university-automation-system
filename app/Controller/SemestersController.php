<?php
App::uses('AppController', 'Controller');
/**
 * Semesters Controller
 *
 * @property Semester $Semester
 * @property PaginatorComponent $Paginator
 */
class SemestersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Semester->recursive = 0;
		$this->set('semesters', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Semester->exists($id)) {
			throw new NotFoundException(__('Invalid semester'));
		}
		$options = array('conditions' => array('Semester.' . $this->Semester->primaryKey => $id));
		$this->set('semester', $this->Semester->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Semester->create();
			if ($this->Semester->save($this->request->data)) {
				$this->Flash->success(__('The semester has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The semester could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Semester->exists($id)) {
			throw new NotFoundException(__('Invalid semester'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Semester->save($this->request->data)) {
				$this->Flash->success(__('The semester has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The semester could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Semester.' . $this->Semester->primaryKey => $id));
			$this->request->data = $this->Semester->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Semester->exists($id)) {
			throw new NotFoundException(__('Invalid semester'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Semester->delete($id)) {
			$this->Flash->success(__('The semester has been deleted.'));
		} else {
			$this->Flash->error(__('The semester could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
