<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Generalsetting', 'Model');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public function beforeFilter(){
        if($this->params['admin']) {
            $this->layout = 'admin';
        } else {
            $obj = new Generalsetting();
            $gen_sett = $obj->find('first');
            #pr($gen_sett);
            $this->set(compact('gen_sett'));
        }
    }
    /*File Upload*/
    public function _upload($file, $folder = '')
    {
        App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));
        if (is_uploaded_file($file['tmp_name'])) {
            $img_arr = explode('.', $file['name']);
            $ext = strtolower(array_pop($img_arr));
            if ($ext == 'txt') $ext = 'jpg';
            $fileName = time() . rand(1, 999) . '.' . $ext;
            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' ||  $ext == 'pdf' ||  $ext == 'doc' ||  $ext == 'docx') {
                $uplodFile = WWW_ROOT . 'files' . DS . $folder . DS . $fileName;                
                if (move_uploaded_file($file['tmp_name'], $uplodFile)) {
                    $dest_small = WWW_ROOT . 'files' . DS . $folder . DS . 'thumb' . DS . $fileName;
                    if ($this->_resize($uplodFile, $dest_small)) {
                        return $fileName;
                    }
                    else die(json_encode(array('success' => false, 'msg' => "Image couldn't resize.")));
                }
            }
        }
    }

    //image resize
    public function _resize($src, $dest_small)
    {
        $phpThumb = new phpThumb();
        $phpThumb->resetObject();
        $capture_raw_data = false;
        $phpThumb->setSourceFilename($src);
        $phpThumb->setParameter('w', 200);
        //$phpThumb->setParameter('w', 1184);
        //$phpThumb->setParameter('h', 852);
        $phpThumb->setParameter('h', 150);
        //$phpThumb->setParameter('zc', 1);
        $phpThumb->GenerateThumbnail();
        $phpThumb->RenderToFile($dest_small);
        return true;
    }
}
