<?php
App::uses('AppController', 'Controller');
/**
 * Notices Controller
 *
 * @property Notice $Notice
 * @property PaginatorComponent $Paginator
 */
class NoticesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Notice->recursive = 0;
		$this->set('notices', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Notice->exists($id)) {
			throw new NotFoundException(__('Invalid notice'));
		}
		$options = array('conditions' => array('Notice.' . $this->Notice->primaryKey => $id));
		$this->set('notice', $this->Notice->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			#pr($this->request->data);
			if(!empty($this->request->data['Notice']['filename']['name'])) {
				$this->request->data['Notice']['filename'] = $this->_upload($this->request->data['Notice']['filename'],'notices');
			} else {
				unset($this->request->data['Notice']['filename']);
			}
            $this->request->data['Notice']['noticeDate'] = date_format(date_create($this->request->data['Notice']['noticeDate']), 'Y-m-d');
			#pr($this->request->data);die;
			$this->Notice->create();
			if ($this->Notice->save($this->request->data)) {
				$this->Flash->success(__('The notice has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The notice could not be saved. Please, try again.'));
			}
		}		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Notice->exists($id)) {
			throw new NotFoundException(__('Invalid notice'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if(!empty($this->request->data['Notice']['filename']['name'])) {
                $this->request->data['Notice']['filename'] = $this->_upload($this->request->data['Notice']['filename'],'notices');
            } else {
                unset($this->request->data['Notice']['filename']);
            }
            $this->request->data['Notice']['noticeDate'] = date_format(date_create($this->request->data['Notice']['noticeDate']), 'Y-m-d');
            $this->Notice->id = $id;
            if ($this->Notice->save($this->request->data)) {
				$this->Flash->success(__('The notice has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The notice could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Notice.' . $this->Notice->primaryKey => $id));
			$this->request->data = $this->Notice->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Notice->exists($id)) {
			throw new NotFoundException(__('Invalid notice'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Notice->delete($id)) {
			$this->Flash->success(__('The notice has been deleted.'));
		} else {
			$this->Flash->error(__('The notice could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
