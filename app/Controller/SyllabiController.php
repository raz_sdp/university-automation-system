<?php
App::uses('AppController', 'Controller');
/**
 * Syllabi Controller
 *
 * @property Syllabus $Syllabus
 * @property PaginatorComponent $Paginator
 */
class SyllabiController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Syllabus->recursive = 0;
		$this->set('syllabi', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Syllabus->exists($id)) {
			throw new NotFoundException(__('Invalid syllabus'));
		}
		$options = array('conditions' => array('Syllabus.' . $this->Syllabus->primaryKey => $id));
		$this->set('syllabus', $this->Syllabus->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(!empty($this->request->data['Syllabus']['file']['name'])) {
				$this->request->data['Syllabus']['file'] = $this->_upload($this->request->data['Syllabus']['file'],'syllabuses');
			} else {
				unset($this->request->data['Syllabus']['file']);
			}
			$this->Syllabus->create();
			if ($this->Syllabus->save($this->request->data)) {
				$this->Flash->success(__('The syllabus has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The syllabus could not be saved. Please, try again.'));
			}
		}
		$departments = $this->Syllabus->Department->find('list', ['fields'=>'Department.departmentName']);
		$this->set(compact('departments'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Syllabus->exists($id)) {
			throw new NotFoundException(__('Invalid syllabus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$old_data = $this->syllabus->findById($id);
			if(!empty($this->request->data['Syllabus']['file']['name'])) {
                $this->request->data['Syllabus']['file'] = $this->_upload($this->request->data['Syllabus']['file'],'syllabuses');
                @unlink(WWW_ROOT . 'files' . DS . 'syllabuses' . DS .$old_data['Syllabus']['file']);
            } else {
                unset($this->request->data['Syllabus']['file']);
            }
            $this->syllabus->id = $id;
			if ($this->Syllabus->save($this->request->data)) {
				$this->Flash->success(__('The syllabus has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The syllabus could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Syllabus.' . $this->Syllabus->primaryKey => $id));
			$this->request->data = $this->Syllabus->find('first', $options);
		}
		$departments = $this->Syllabus->Department->find('list', ['fields'=>'Department.departmentName']);
		$this->set(compact('departments'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Syllabus->exists($id)) {
			throw new NotFoundException(__('Invalid syllabus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Syllabus->delete($id)) {
			$this->Flash->success(__('The syllabus has been deleted.'));
		} else {
			$this->Flash->error(__('The syllabus could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
