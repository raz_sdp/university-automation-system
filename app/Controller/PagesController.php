<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('Notice', 'Model');
App::uses('News', 'Model');
App::uses('Slider', 'Model');
App::uses('Faculty', 'Model');
App::uses('Facility', 'Model');
App::uses('Glance', 'Model');
App::uses('Syllabus', 'Model');
App::uses('Activity', 'Model');
App::uses('Semester', 'Model');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		if (in_array('..', $path, true) || in_array('.', $path, true)) {
			throw new ForbiddenException();
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}

		/*Pick Data for Home page*/
		if($page=='home') {
			$notice_obj = new Notice();
			$query = [
				'fields' => [
					'Notice.id', 'Notice.title', 'Notice.filename', 'Notice.noticeDate', 'Notice.type'
				],				
				'order' => ['Notice.id' => 'DESC']
			];
			$notices = $notice_obj->find('all', $query);
			/*News*/
			$news_obj = new News();
			$news = $news_obj->find('all');
			/*Sliders*/
			$slider_obj = new Slider();
			$sliders = $slider_obj->find('all');
			/*Facility*/
			$facility_obj = new Facility();
			$facilities = $facility_obj->find('all');
			/*Glance*/
			$glance_obj = new Glance();
			$glances = $glance_obj->find('all');
			
			$this->set(compact('notices','news', 'sliders', 'glances', 'facilities'));
		}
		/*Pick Data for About page*/
		if($page=='about') {
			/*Faculty*/
			$faculty_obj = new Faculty();
			$faculties = $faculty_obj->find('all');
			
			$this->set(compact('faculties'));

		}

		// /*Pick Data for Academic page*/
		// if($page=='academic') {
		// 	/*Academic Program*/
		// 	$faculty_obj = new Faculty();
		// 	$faculties = $faculty_obj->find('all');

		// 	/*Academic Calendar*/
		// 	$activity_obj = new Activity();
		// 	$activities = $activity_obj->find('all');

		// 	$semester_obj = new Semester();
		// 	$semesters = $semester_obj->find('all');

		// 	/*Syllabus*/
		// 	$sylabus_obj = new Syllabus();
		// 	$syllabuses = $sylabus_obj->find('all');
			
		// 	$this->set(compact('faculties','syllabuses','activities','semesters'));

		// }
		/*Pick Data for Notice & News page*/
		if($page=='notices-news') {
			/*All Notices*/
			$notice_obj = new Notice();
			$query = [
				'fields' => [
					'Notice.id', 'Notice.title', 'Notice.filename', 'Notice.noticeDate', 'Notice.type'
				],				
				'order' => ['Notice.id' => 'DESC']
			];
			$notices = $notice_obj->find('all', $query);

			/*All News & Event*/
			$news_obj = new News();
			$news = $news_obj->find('all');
			
			$this->set(compact('notices','news'));

		}
		/*Pick Data for Admission Undergraduate page*/
		if($page=='undergraduate') {
			/*All Notices*/
			$notice_obj = new Notice();
			$query = [
				'fields' => [
					'Notice.id', 'Notice.title', 'Notice.filename', 'Notice.noticeDate', 'Notice.type'
				],				
				'order' => ['Notice.id' => 'DESC']
			];
			$notices = $notice_obj->find('all', $query);
			
			$this->set(compact('notices'));

		}

		/*Pick Data for Admission Postgraduate*/
		if($page=='postgraduate') {
			/*All Notices*/
			$notice_obj = new Notice();
			$query = [
				'fields' => [
					'Notice.id', 'Notice.title', 'Notice.filename', 'Notice.noticeDate', 'Notice.type'
				],				
				'order' => ['Notice.id' => 'DESC']
			];
			$notices = $notice_obj->find('all', $query);
			
			$this->set(compact('notices'));

		}

		/*Pick Data for academic programs page*/
		if($page=='ac_programs') {
			/*Academic Program*/
			$faculty_obj = new Faculty();
			$faculties = $faculty_obj->find('all');
			
			$this->set(compact('faculties'));

		}

		/*Pick Data for academic calendar page*/
		if($page=='ac_calendar') {
			/*Academic Program*/
			$semester_obj = new Semester();
			$semesters = $semester_obj->find('all');
			
			$this->set(compact('semesters'));

		}

		/*Pick Data for Academic syllabuses page*/
		if($page=='ac_syllabuses') {
			/*Syllabus*/
			$sylabus_obj = new Syllabus();
			$syllabuses = $sylabus_obj->find('all');
			
			$this->set(compact('syllabuses'));

		}


		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
}
