<?php
App::uses('AppController', 'Controller');
/**
 * Faculties Controller
 *
 * @property Faculty $Faculty
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class FacultiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Faculty->recursive = 0;
		$this->set('faculties', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Faculty->exists($id)) {
			throw new NotFoundException(__('Invalid faculty'));
		}
		$options = array('conditions' => array('Faculty.' . $this->Faculty->primaryKey => $id));
		$this->set('faculty', $this->Faculty->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(!empty($this->request->data['Faculty']['facultyImage']['name'])){
				$this->request->data['Faculty']['facultyImage'] = $this-> _upload($this->request->data['Faculty']['facultyImage'],'faculties');

			} else {
				unset($this->request->data['Faculty']['facultyImage']);
			}


			$this->Faculty->create();
			if ($this->Faculty->save($this->request->data)) {
				$this->Flash->success(__('The faculty has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The faculty could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Faculty->exists($id)) {
			throw new NotFoundException(__('Invalid faculty'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$old_data = $this->Faculty->findById($id);
			if(!empty($this->request->data['Faculty']['facultyImage']['name'])){
				$this->request->data['Faculty']['facultyImage'] = $this-> _upload($this->request->data['Faculty']['facultyImage'],'faculties');
				@unlink(WWW_ROOT , 'files' . DS . 'faculties' . DS . $old_data['Faculty']['facultyImage']);

			} else {
				unset($this->request->data['Faculty']['facultyImage']);
			}
			if ($this->Faculty->save($this->request->data)) {
				$this->Flash->success(__('The faculty has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The faculty could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Faculty.' . $this->Faculty->primaryKey => $id));
			$this->request->data = $this->Faculty->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Faculty->exists($id)) {
			throw new NotFoundException(__('Invalid faculty'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Faculty->delete($id)) {
			$this->Flash->success(__('The faculty has been deleted.'));
		} else {
			$this->Flash->error(__('The faculty could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
