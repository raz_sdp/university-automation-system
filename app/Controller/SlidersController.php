<?php
App::uses('AppController', 'Controller');
/**
 * Sliders Controller
 *
 * @property Slider $Slider
 * @property PaginatorComponent $Paginator
 */
class SlidersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Slider->recursive = 0;
		$this->set('sliders', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		$options = array('conditions' => array('Slider.' . $this->Slider->primaryKey => $id));
		$this->set('slider', $this->Slider->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(!empty($this->request->data['Slider']['img']['name'])) {
				$this->request->data['Slider']['img'] = $this->_upload($this->request->data['Slider']['img'],'sliders');
			} else {
				unset($this->request->data['Slider']['img']);
			}
			$this->Slider->create();
			if ($this->Slider->save($this->request->data)) {
				$this->Flash->success(__('The slider has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The slider could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$old_data = $this->Slider->findById($id);
			if(!empty($this->request->data['Slider']['img']['name'])) {
				$this->request->data['Slider']['img'] = $this->_upload($this->request->data['Slider']['img'],'sliders');
				@unlink(WWW_ROOT . 'files' . DS . 'sliders' . DS .$old_data['Slider']['img']);
                @unlink(WWW_ROOT . 'files' . DS . 'sliders' . DS .'thumb'.$old_data['Slider']['img']);
			} else {
				unset($this->request->data['Slider']['img']);
			}

            $this->Slider->id = $id;
			if ($this->Slider->save($this->request->data)) {
				$this->Flash->success(__('The slider has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The slider could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Slider.' . $this->Slider->primaryKey => $id));
			$this->request->data = $this->Slider->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Slider->exists($id)) {
			throw new NotFoundException(__('Invalid slider'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Slider->delete($id)) {
			$this->Flash->success(__('The slider has been deleted.'));
		} else {
			$this->Flash->error(__('The slider could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
