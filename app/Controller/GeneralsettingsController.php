<?php
App::uses('AppController', 'Controller');
/**
 * Generalsettings Controller
 *
 * @property Generalsetting $Generalsetting
 * @property PaginatorComponent $Paginator
 */
class GeneralsettingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Generalsetting->recursive = 0;
		$this->set('generalsettings', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Generalsetting->exists($id)) {
			throw new NotFoundException(__('Invalid generalsetting'));
		}
		$options = array('conditions' => array('Generalsetting.' . $this->Generalsetting->primaryKey => $id));
		$this->set('generalsetting', $this->Generalsetting->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if(!empty($this->request->data['Generalsetting']['logo']['name'])){
				$this->request->data['Generalsetting']['logo'] = $this->_upload($this->request->data['Generalsetting']['logo'],'generalsetting');
			} else {
				unset($this->request->data['Generalsetting']['logo']);
			}
			$this->Generalsetting->create();
			if ($this->Generalsetting->save($this->request->data)) {
				$this->Flash->success(__('The generalsetting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The generalsetting could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Generalsetting->exists($id)) {
			throw new NotFoundException(__('Invalid generalsetting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$old_data = $this->Generalsetting->findById($id);
			if(!empty($this->request->data['Generalsetting']['logo']['name'])) {
                $this->request->data['Generalsetting']['logo'] = $this->_upload($this->request->data['Generalsetting']['logo'],'generalsetting');
                @unlink(WWW_ROOT . 'files' . DS . 'generalsetting' . DS .$old_data['Generalsetting']['logo']);
                
            } else {
                unset($this->request->data['Generalsetting']['logo']);
            }
            $this->Generalsetting->id = $id;
			if ($this->Generalsetting->save($this->request->data)) {
				$this->Flash->success(__('The generalsetting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The generalsetting could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Generalsetting.' . $this->Generalsetting->primaryKey => $id));
			$this->request->data = $this->Generalsetting->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Generalsetting->exists($id)) {
			throw new NotFoundException(__('Invalid generalsetting'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Generalsetting->delete($id)) {
			$this->Flash->success(__('The generalsetting has been deleted.'));
		} else {
			$this->Flash->error(__('The generalsetting could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
