<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
Router::connect('/about', array('controller' => 'pages', 'action' => 'display', 'about'));
Router::connect('/contact', array('controller' => 'pages', 'action' => 'display', 'contact'));
Router::connect('/academic', array('controller' => 'pages', 'action' => 'display', 'academic'));
Router::connect('/undergraduate', array('controller' => 'pages', 'action' => 'display', 'undergraduate'));
Router::connect('/postgraduate', array('controller' => 'pages', 'action' => 'display', 'postgraduate'));
Router::connect('/diploma', array('controller' => 'pages', 'action' => 'display', 'diploma'));
Router::connect('/foreign', array('controller' => 'pages', 'action' => 'display', 'foreign'));
Router::connect('/ac_programs', array('controller' => 'pages', 'action' => 'display', 'ac_programs'));
Router::connect('/ac_system', array('controller' => 'pages', 'action' => 'display', 'ac_system'));
Router::connect('/ac_regulations', array('controller' => 'pages', 'action' => 'display', 'ac_regulations'));
Router::connect('/ac_calendar', array('controller' => 'pages', 'action' => 'display', 'ac_calendar'));
Router::connect('/ac_syllabuses', array('controller' => 'pages', 'action' => 'display', 'ac_syllabuses'));
Router::connect('/vm', array('controller' => 'pages', 'action' => 'display', 'vm'));
Router::connect('/notices-news', array('controller' => 'pages', 'action' => 'display', 'notices-news'));
Router::connect('/dept', array('controller' => 'pages', 'action' => 'display', 'dept'));
Router::connect('/dept-profile', array('controller' => 'pages', 'action' => 'display', 'dept-profile'));
Router::connect('/massage-vc', array('controller' => 'pages', 'action' => 'display', 'massage-vc'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
