<?php
App::uses('AppModel', 'Model');
/**
 * Faculty Model
 *
 * @property Department $Department
 */
class Faculty extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Department' => array(
			'className' => 'Department',
			'foreignKey' => 'faculty_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
