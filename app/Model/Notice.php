<?php
App::uses('AppModel', 'Model');
/**
 * Notice Model
 *
 */
class Notice extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'notice';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}
