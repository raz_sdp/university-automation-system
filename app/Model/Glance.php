<?php
App::uses('AppModel', 'Model');
/**
 * Glance Model
 *
 */
class Glance extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'glance';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}
