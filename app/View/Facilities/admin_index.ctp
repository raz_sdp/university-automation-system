<div class="facilities index">
	<h2><?php echo __('Facilities'); ?></h2>
	<table class="table table-hover" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('medicalDesc'); ?></th>
			<th><?php echo $this->Paginator->sort('cafeDesc'); ?></th>
			<th><?php echo $this->Paginator->sort('auditoriamDesc'); ?></th>
			<th><?php echo $this->Paginator->sort('transportDesc'); ?></th>
			<th><?php echo $this->Paginator->sort('wifiDesc'); ?></th>
			<th><?php echo $this->Paginator->sort('libraryDesc'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($facilities as $facility): ?>
	<tr>
		<td><?php echo h($facility['Facility']['id']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['medicalDesc']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['cafeDesc']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['auditoriamDesc']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['transportDesc']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['wifiDesc']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['libraryDesc']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['created']); ?>&nbsp;</td>
		<td><?php echo h($facility['Facility']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $facility['Facility']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $facility['Facility']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $facility['Facility']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $facility['Facility']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Facility'), array('action' => 'add')); ?></li>
	</ul>
</div>
