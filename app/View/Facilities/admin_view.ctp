<div class="facilities view">
<h2><?php echo __('Facility'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MedicalDesc'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['medicalDesc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CafeDesc'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['cafeDesc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AuditoriamDesc'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['auditoriamDesc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('TransportDesc'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['transportDesc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('WifiDesc'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['wifiDesc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LibraryDesc'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['libraryDesc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($facility['Facility']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Facility'), array('action' => 'edit', $facility['Facility']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Facility'), array('action' => 'delete', $facility['Facility']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $facility['Facility']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Facilities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facility'), array('action' => 'add')); ?> </li>
	</ul>
</div>
