<section class="forms">
    <div class="container-fluid section-top">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-header d-flex align-items-center custom-bg-color text-white">
                        <h3 class="h4">Edit Facility</h3>
                    </div>
                    <div class="card-body">
                        <?php echo $this->Form->create('Facility',array('enctype' => 'multipart/form-data')); ?>
                        <?php echo $this->Form->input('id'); ?> 
                         <div class="form-group">
                            <?php echo $this->Form->input('medicalDesc',['label'=> 'Medical Description','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('cafeDesc',['label'=> 'Cafeteria Description','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('auditoriamDesc',['label'=> 'Auditriam Description','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('transportDesc',['label'=> 'Transport Description','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('wifiDesc',['label'=> 'WiFi Description','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('libraryDesc',['label'=> 'Library Description','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         
                        <div class="row">
                            <div class="offset-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block">Update Facility</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

