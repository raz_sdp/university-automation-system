<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2><b>UNDERGRADUATE</b> ADMISSION</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-6 col-sm-12">
				<ul class="list-inline pt-4 float-right list-inline-custom">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item "> / Admission  /</li>
					<li class="nav-item list-inline-item ">Undergraduate</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<?php
		foreach ($notices as $key => $notice) {
			if($notice['Notice']['type'] == 'Undergraduate Admission'){
		?>
		<div class="undergraduate-sub">
			<p class="pt-3"><?=$notice['Notice']['title']?></p>
			<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
		</p>
		<?php
		if($key<count($notices)-1) echo '<div class="border"></div>';
		?>
	</div>
	<?php
	}
	}
	?>
	<ul class="pagination float-right pt-4">
		<li class="page-item"><a class="page-link" href="#">Previous</a></li>
		<li class="page-item"><a class="page-link" href="#">1</a></li>
		<li class="page-item"><a class="page-link" href="#">2</a></li>
		<li class="page-item"><a class="page-link" href="#">Next</a></li>
	</ul>
</div>
</div>
<!-- <div class="title-about">
<div class="container">
		<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12">
						<h2>ADMISSION</h2>
						<p>Northern University Khulna</p>
				</div>
				<div class="col-lg-8 col-md-6 col-sm-12">
						<ul class="list-inline pt-4 float-right list-inline-custom">
								<li class="nav-item list-inline-item"><a href="#">Home </a></li>
								<li class="nav-item list-inline-item "> / Admission  /</li>
								<li class="nav-item list-inline-item ">Undergraduate</li>
						</ul>
				</div>
		</div>
</div>
</div>
<div class="about-right">
<div class="container p-5">
		<div class="row">
				<div class="col-lg-4 col-md-12 col-sm-12" data-aos="fade-right">
						<div class="list-group mb-5" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action custom-left active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home"> Undergraduate  <span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span></a>
								<a class="list-group-item list-group-item-action custom-left" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile"> post Graduate <span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span>
						</a>
						<a class="list-group-item list-group-item-action custom-left" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages"> Diploma <span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span></a>
						<a class="list-group-item list-group-item-action custom-left" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings"> Foreign students <span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span>
				</a>
		</div>
</div>
<div class="col-lg-8 col-md-12 col-sm-12 " data-aos="fade-left">
		<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
						<h3 class="">UNDERGRADUATE ADMISSION</h3>
						<p>NUB has been expanding its academic programs at the Bachelors and Masters levels under different faculties and departments . At present, the university’s academic programs are as follows :</p>
						<div class="admission p-3 shadow overflow-hidden">
				<?php
				foreach ($notices as $key => $notice) {
					if($notice['Notice']['type'] == 'Undergraduate Admission'){
				?>
				<div class="undergraduate-sub">
					<p class="pt-3"><?=$notice['Notice']['title']?></p>
					<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
				</p>
				<?php
				if($key<count($notices)-1) echo '<div class="border"></div>';
				?>
			</div>
			<?php
			}
			}
			?>
		</div>
	</div>
	<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
		<h3>POSTGRADUATE ADMISSION</h3>
		<div class="undergraduate border p-3 shadow overflow-hidden">
			<?php
			foreach ($notices as $key => $notice) {
				if($notice['Notice']['type'] == 'Postgraduate Admission'){
			?>
			<div class="undergraduate-sub">
				<p class="pt-3"><?=$notice['Notice']['title']?></p>
				<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
			</p>
			<?php
			if($key<count($notices)-1) echo '<div class="border"></div>';
			?>
		</div>
		<?php
		}
		}
		?>
	</div>
</div>
<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
	<h3>DIPLOMA</h3>
	<p>An academic year is divided into three semesters.<br>
		Each semester is of 16 weeks duration, which is distributed as follows:<br>
		14 weeks for instruction, 2-weeks for examination and break.<br>
		The schedules for three semesters are as follows.
		An academic year is divided into three semesters.<br>
		Each semester is of 16 weeks duration, which is distributed as follows:<br>
		14 weeks for instruction, 2-weeks for examination and break.<br>
		The schedules for three semesters are as follows.
	</p>
</div>
<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
	<h3>ADMISSION FOR FOREING STUDENTS</h3>
	<P>Interested foreign students are requested to contact with the admission office (registrar[at]just.edu.bd; admission@just.edu.bd) with prescribed application form & all academic qualifications before the deadline each year.</P>
	<div class="card  mt-4 mb-4 shadow">
		<div class="card-header">
			<strong>Undergraduate Tuition fees (this may vary from year to year):</strong>
		</div>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">1. USD 1000 per year – For SAARC countries & NRB</li>
			<li class="list-group-item">2. USD 1400 per year – For other countries</li>
		</ul>
	</div>
	<div class="card  mt-4 mb-4 shadow">
		<div class="card-header">
			<strong>Graduate Tuition fees (this may vary from year to year):</strong>
		</div>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">1. USD 1400-1600 per year – For SAARC countries & NRB</li>
			<li class="list-group-item">2. USD 1600-2000 per year For other countries</li>
		</ul>
	</div>
	<div class="card mt-4 mb-4 shadow">
		<div class="card-header">
			<strong>Living cost:</strong>
		</div>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Approximately $150 to $300 per month (depends on individual life style). Note that there is no scope of part time job either on campus or off campus</li>
		</ul>
	</div>
	<div class="card  mt-4 mb-4 shadow">
		<div class="card-header">
			<strong>Admission requirements</strong>
		</div>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">– Applicant must be a foreigner or son/daughter of any Non Resident Bangladeshi (NRB) studied abroad.</li>
			<li class="list-group-item">– Applicants should have completed 12 years of schooling or equivalent qualification.</li>
			<li class="list-group-item">– It is essential that your English language skills are good enough for you to undertake an intensive and challenging academic course that is taught and examined in English.</li>
			<div class="card-header">
				As a result if your first language is not English and if you did not conducted your previous study in English, you will be asked to attend an online viva (i.e. Skype, etc.) arranged by JUST authority to test your English skill.
			</div>
		</ul>
	</div>
	<div class="card  mt-4 mb-4 shadow">
		<div class="card-header">
			<strong>Admission requirements</strong>
		</div>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">– Applicant must be a foreigner or son/daughter of any Non Resident Bangladeshi (NRB) studied abroad.</li>
			<li class="list-group-item">– Applicants should have completed 16 years of schooling or equivalent degree including an undergraduate University degree.</li>
			<li class="list-group-item">– It is essential that your English language skills are good enough for you to undertake an intensive and challenging academic courses and research those are taught and examined in English. As a result if your first language is not English and if you did not conducted your previous studies in English, you will be asked to attend an online viva (i.e. Skype, etc.) arranged by JUST authority to test your English skill.</li>
			<li class="list-group-item">– International graduate applications are evaluated on case by case basis considering different parameters.</li>
			<div class="card-header">
				It is to be noted that the JUST authority will take necessary steps to verify the authenticity of the submitted documents. If any document is proven wrong/fake at any stage of admission or study period, admission will be canceled immediately.
			</div>
		</ul>
	</div>
</div>
</div>
</div>
</div>
</div>
</div> -->