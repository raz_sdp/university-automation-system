<section class="">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
				<div id="wowslider-container1">
					<div class="ws_images">
						<ul>
							<?php
							foreach ($sliders as $key => $slider) {
								?>
								<li><img class="text-center" src="<?=$this->html->url('/files/sliders/'.$slider['Slider']['img'])?>" alt="033" title="<?=$slider['Slider']['title']?>" id="wows1_0" />
								</li>
								<?php
							}
							?>
						</ul>
					</div>
					<div class="ws_thumbs">
						<div>
							<?php
							foreach ($sliders as $key => $slider) {
								?>
								<a href="#" title="033"><img src="<?=$this->html->url('/files/sliders/thumb/'.$slider['Slider']['img'])?>" alt="" /></a>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<div id="sp-title" class="col-lg-4 col-md-12 col-sm-12 col-xs-12" data-aos="flip-left"
			data-aos-easing="ease-out-cubic"
			data-aos-duration="2000">
			<div class="card mb-1">
				<h3 class="card-header animated flash text-uppercase text-center font-weight-bold ">Welcome to KUET</h3>
				<div class="">
					<p><?php echo substr($gen_sett['Generalsetting']['welcomeMsg'],0,195);?> <a href="<?php echo $this->html->url('/about')?>" class="">see more..</a></p>
				</div>
			</div>
			<div class="border p-1 mb-1">
				<img class="" src="img/principal.png" alt="Card image cap" width="80" height="80">
				<span class="principal"><strong>Message from principal </strong></span>
				<div class="message">
					<p class="card-text p-2"><?php echo substr($gen_sett['Generalsetting']['principalMsg'],0,75);?>  <a href="<?php echo $this->html->url('/massage-vc')?>" class="">see more..</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<section id="news-notice">
	<div class="container">
		<div class="row pt-4 pb-4">
			<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
				<div class="rounded card-1 shadow mb-4">
					<h3 class="p-4 text-center section-title"><strong class="str">NOTICE</strong>BOARD</h3>
					<div class="scrollbox ">
						<div class="text-white scrollbox-content">
							<?php
							foreach ($notices as $key => $notice) {
								?>
								<div class="p-3">
									<p><?=$notice['Notice']['title']?></p>
									<p class="notice-pra "><i class="fas fa-calendar-alt"></i><?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?> <span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span></p>
									<?php
									if($key<count($notices)-1) echo '<div class="border"></div>';
									?>
								</div>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 notice-mob-dev">
				<h3 class="p-4 section-title"><strong class="str">NEWS</strong> & EVENTS</h3>
				<div class="scrollbox">
					<div class=" card-2 scrollbox-content text-white">
						<?php
						foreach ($news as $key => $sin_news) {
						//pr($news);die;
							?>
							<div class=" row p-2">
								<div class="col-lg-3 col-md-5 col-sm-12">
									<a href=""><img src="<?php echo $this->Html->url('/files/news/thumb/'.$sin_news['News']['img'])?>" class="img-fluid img-thumbnail news-mob"></a>
								</div>
								<div class="col-lg-9 col-md-7 col-sm-12">
									<a class="text-white font-weight-bold" href="">
										<p><?=$sin_news['News']['title']?></p>
									</a>
									<p class="notice-pra"><i class="far fa-calendar-plus"></i> <?=date_format(date_create($sin_news['News']['created']), 'd M Y'); ?></p>
								</div>
							</div>
							<?php
						}
						?>
					</div>
					<button class="btn btn-block float-right btn-news">SEE ALL</button>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="facilities" class="facilities pt-4 pb-4">
	<div class="container">
		<h2 class="text-center p-4 section-title"><strong class="str">JUST</strong> FACILITIES</h2>
		<div class="owl-carousel owl-theme pt-4 pb-4">
			<?php
			foreach ($facilities as $key => $facility) {
					#pr($facility);die;
				?>
				<div class="item bg-white p-4 card" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting p-4 text-center size7x"></i>
					<h4 class="pt-3 pb-3 section-title">24/7 on campus <strong class="str">medical services</strong></h4>
					<p><?=$facility['Facility']['medicalDesc']?> <a href="#">read more..</a></p>
				</div>
				<div class="item bg-white p-4 card" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting p-4 text-center size7x"></i>
					<h4 class="pt-3 pb-3 section-title">About <strong class="str">central cafeteria</strong> and food</h4>
					<p><?=$facility['Facility']['cafeDesc']?> <a href="#">read more..</a></p>
				</div>
				<div class="item bg-white p-4 card" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting p-4 text-center size7x"></i>
					<h4 class="pt-3 pb-3 section-title">Central <strong class="str">auditorium</strong> for conferences</h4>
					<p><?=$facility['Facility']['auditoriamDesc']?> <a href="#">read more..</a></p>
				</div>
				<div class="item bg-white p-4 card" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting p-4 text-center size7x"></i>
					<h4 class="pt-3 pb-3 section-title">university <strong class="str">transport facilities</strong></h4>
					<p><?=$facility['Facility']['transportDesc']?><a href="#">read more..</a></p>
				</div>
				<div class="item bg-white p-4 card" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting p-4 text-center size7x"></i>
					<h4 class="pt-3 pb-3 section-title"><strong class="str">Free wifi</strong> coverage throughout the campus</h4>
					<p><?=$facility['Facility']['wifiDesc']?> <a href="#">read more..</a></p>
				</div>
				<div class="item bg-white p-4 card" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting p-4 text-center size7x"></i>
					<h4 class="pt-3 pb-3 section-title"><strong class="str">Digital</strong> Library &amp; research center</h4>
					<p><?=$facility['Facility']['libraryDesc']?> <a href="#">read more..</a></p>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</section>
<section id="university-glance" class="university-glance text-center p-4">
	<div class="container">
		<h2 class="section-title p-4"><strong class="str">OUR UNIVERSITY</strong> AT A GLANCE</h2>
		<div class="row glance pt-4 pb-4">
			<?php
			foreach ($glances as $key => $glance) {
				#pr($glance);die;
				?>
				<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<?=$glance['Glance']['icon']?>
					<h4 class="p-3"><?=$glance['Glance']['title']?></h4>
					<p><?=$glance['Glance']['description']?></p>
				</div>
				<?php
			}
			?>
			<!--
			<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-ambulance size7x"></i>
					<h4 class="p-3">420</h4>
					<p>Lorem Ipsum</p>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fab fa-accessible-icon size7x"></i>
					<h4 class="p-3">420</h4>
					<p>Lorem Ipsum</p>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fas fa-american-sign-language-interpreting size7x"></i>
					<h4 class="p-3">420</h4>
					<p>Lorem Ipsum</p>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fab fa-apple-pay size7x"></i>
					<h4 class="p-3">420</h4>
					<p>Lorem Ipsum</p>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
					<i class="fab fa-accusoft size7x"></i>
					<h4 class="p-3">420</h4>
					<p>Lorem Ipsum</p>
				</div> -->
			</div>
		</div>
	</section>