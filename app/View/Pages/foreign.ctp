<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2>ADMISSION FOR FOREIGN STUDENS</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-6 col-sm-12">
				<ul class="list-inline pt-4 float-right list-inline-custom">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item "> / Admission  /</li>
					<li class="nav-item list-inline-item ">Foreign</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container contant" data-aos="fade-right">
	<div class="admission p-3 shadow overflow-hidden">
		<P>Interested foreign students are requested to contact with the admission office (registrar[at]just.edu.bd; admission@just.edu.bd) with prescribed application form & all academic qualifications before the deadline each year.</P>
		<div class="card  mt-4 mb-4 shadow">
			<div class="card-header">
				<strong>Undergraduate Tuition fees (this may vary from year to year):</strong>
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">1. USD 1000 per year – For SAARC countries & NRB</li>
				<li class="list-group-item">2. USD 1400 per year – For other countries</li>
			</ul>
		</div>
		<div class="card  mt-4 mb-4 shadow">
			<div class="card-header">
				<strong>Graduate Tuition fees (this may vary from year to year):</strong>
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">1. USD 1400-1600 per year – For SAARC countries & NRB</li>
				<li class="list-group-item">2. USD 1600-2000 per year For other countries</li>
			</ul>
		</div>
		<div class="card mt-4 mb-4 shadow">
			<div class="card-header">
				<strong>Living cost:</strong>
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Approximately $150 to $300 per month (depends on individual life style). Note that there is no scope of part time job either on campus or off campus</li>
			</ul>
		</div>
		<div class="card  mt-4 mb-4 shadow">
			<div class="card-header">
				<strong>Admission requirements</strong>
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">– Applicant must be a foreigner or son/daughter of any Non Resident Bangladeshi (NRB) studied abroad.</li>
				<li class="list-group-item">– Applicants should have completed 12 years of schooling or equivalent qualification.</li>
				<li class="list-group-item">– It is essential that your English language skills are good enough for you to undertake an intensive and challenging academic course that is taught and examined in English.</li>
				<div class="card-header">
					As a result if your first language is not English and if you did not conducted your previous study in English, you will be asked to attend an online viva (i.e. Skype, etc.) arranged by JUST authority to test your English skill.
				</div>
			</ul>
		</div>
		<div class="card  mt-4 mb-4 shadow">
			<div class="card-header">
				<strong>Admission requirements</strong>
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">– Applicant must be a foreigner or son/daughter of any Non Resident Bangladeshi (NRB) studied abroad.</li>
				<li class="list-group-item">– Applicants should have completed 16 years of schooling or equivalent degree including an undergraduate University degree.</li>
				<li class="list-group-item">– It is essential that your English language skills are good enough for you to undertake an intensive and challenging academic courses and research those are taught and examined in English. As a result if your first language is not English and if you did not conducted your previous studies in English, you will be asked to attend an online viva (i.e. Skype, etc.) arranged by JUST authority to test your English skill.</li>
				<li class="list-group-item">– International graduate applications are evaluated on case by case basis considering different parameters.</li>
				<div class="card-header">
					It is to be noted that the JUST authority will take necessary steps to verify the authenticity of the submitted documents. If any document is proven wrong/fake at any stage of admission or study period, admission will be canceled immediately.
				</div>
			</ul>
		</div>
	</div>
</div>