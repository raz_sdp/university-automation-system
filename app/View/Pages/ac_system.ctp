<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>ACADEMIC SYSTEM</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-8 ">
				<ul class="list-inline pt-4 list-inline-custom float-right">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item"> / Academic  /</li>
					<li class="nav-item list-inline-item">Academic System</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<h3>ACADEMIC SYSTEM</h3>
		<h6>The Credit Hour System</h6>
		<p>The Credit Hour System is followed in the academic programs of NUB. Courses may be of different credit hours. Each credit hour implies 14 classes hours per semester. Thus a 3 credit hour course requires 42 class hours. NUB also introduces open credit hour system in its academic system. The open credit hour system provides flexibility to the students to complete required number of credit hours for a program, in which the number of credit hours in different semesters depend on a student choice and ability.</p>
		<h6>The Evaluation System</h6>
		<p>The evaluation system practiced at NUB is based on 100 marks for each course. The process of evaluation is based on the following activities :</p>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Evaluation Type</th>
					<th scope="col">Marks</th>
					<th scope="col">Examination Type</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">01</th>
					<td>Continuous Assessment</td>
					<td>30 Marks</td>
					<td>Class tests, assignments, quizzes, case studies & class participation.</td>
				</tr>
				<tr>
					<th scope="row">02</th>
					<td>Mid-term Examination</td>
					<td>30 Marks</td>
					<td>Held in the middle of the semester</td>
				</tr>
				<tr>
					<th scope="row">03</th>
					<td>Final Examination</td>
					<td>40 Marks</td>
					<td>Held at the end of the semester</td>
				</tr>
				<tr>
					<td colspan="2">TOTAL</td>
					<td colspan="2">100 Marks</td>
				</tr>
			</tbody>
		</table>
		<h6>Grading : </h6>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th scope="col">Numerical Grade</th>
					<th scope="col">Letter Grade</th>
					<th scope="col">GP</th>
					<th scope="col">#</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td scope="row">80% and above</td>
					<td>A+</td>
					<td>(A plus)</td>
					<td>4.0</td>
				</tr>
				<tr>
					<td scope="row">75% to less than 80%</td>
					<td>A</td>
					<td>(A regular)</td>
					<td>3.75</td>
				</tr>
				<tr>
					<td scope="row">70% to less than 75%</td>
					<td>A-</td>
					<td>(A minus)</td>
					<td>3.50</td>
				</tr>
				<tr>
					<td>65% to less than 70%</td>
					<td>B+</td>
					<td>(B plus)</td>
					<td>3.25</td>
				</tr>
				<tr>
					<td>60% to less than 65% </td>
					<td>B</td>
					<td>(B regular) </td>
					<td>3.00</td>
				</tr>
				<tr>
					<td>55% to less than 60%</td>
					<td>B-</td>
					<td>(B minus)</td>
					<td>2.75</td>
				</tr>
				<tr>
					<td>50% to less than 55%</td>
					<td>C+</td>
					<td>(C plus)</td>
					<td>2.50</td>
				</tr>
				<tr>
					<td>45% to less than 50%</td>
					<td>C</td>
					<td>(C regular)</td>
					<td>2.25</td>
				</tr>
				<tr>
					<td>40% to less than 45%</td>
					<td>D</td>
					<td>D</td>
					<td>2.0</td>
				</tr>
				<tr>
					<td>Less than 40%</td>
					<td>F</td>
					<td>F</td>
					<td>0.0</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>