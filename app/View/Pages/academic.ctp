<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>ACADEMIC</h2>
				<p>Northern University Bangladesh</p>
			</div>
			<div class="col-md-8 ">
				<ul class="list-inline pt-4 list-inline-custom float-right">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item"> / Academic  /</li>
					<li class="nav-item list-inline-item">Academic Programs</li>					
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="about-right">
	<div class="container p-5">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-sm-12 pb-5" data-aos="fade-right">
				<div class="list-group" id="list-tab" role="tablist">
					<a class="list-group-item list-group-item-action custom-left active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Academic Programs <span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span></a>

					<a class="list-group-item list-group-item-action custom-left" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Academic System<span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span>
					</a>

					<a class="list-group-item list-group-item-action custom-left" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Academic Regulations <span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span></a>

					<a class="list-group-item list-group-item-action custom-left" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Academic Calendar<span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span>
					</a>

					<a class="list-group-item list-group-item-action custom-left" id="list-faculties-list" data-toggle="list" href="#list-faculties" role="tab" aria-controls="faculties">Syllabus<span class="float-right mob-arrow"><i class="fas fa-arrow-right"></i></span>

					</a>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-sm-12" data-aos="fade-left">
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
						<h3 class="">ACADEMIC PROGRAMS</h3>
						<p>NUB has been expanding its academic programs at the Bachelors and Masters levels under different faculties and departments . At present, the university’s academic programs are as follows :</p>

						<div class="row">
							<?php 
							foreach ($faculties as $key => $faculty) {
								?>
								<div class="col-md-6 ">
									<div class="academic-program">
										<img src="<?php echo $this->html->url('/files/faculties/'.$faculty['Faculty']['facultyImage'])?>" alt="program" class="img-fluid img-thumbnail">
										<!-- <img src="img/program.jpg" alt="program" class="img-fluid img-thumbnail"> -->
										<h6><?=$faculty['Faculty']['facultyName']?></h6>
										<div class="">
											<ul>
												<?php 
												foreach ($faculty['Department'] as $dept) {
													?>
													<li><a href="#"><?=$dept['departmentName']?></a></li>
													<?php
												}
												?>
											</ul>
										</div>
									</div>
								</div>
							<?php
							}
							 ?>
						</div>

					</div>
					<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
						<h3>ACADEMIC SYSTEM</h3>
						<h6>The Credit Hour System</h6>
						<p>The Credit Hour System is followed in the academic programs of NUB. Courses may be of different credit hours. Each credit hour implies 14 classes hours per semester. Thus a 3 credit hour course requires 42 class hours. NUB also introduces open credit hour system in its academic system. The open credit hour system provides flexibility to the students to complete required number of credit hours for a program, in which the number of credit hours in different semesters depend on a student choice and ability.</p>
						<h6>The Evaluation System</h6>
						<p>The evaluation system practiced at NUB is based on 100 marks for each course. The process of evaluation is based on the following activities :</p>
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Evaluation Type</th>
									<th scope="col">Marks</th>
									<th scope="col">Examination Type</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">01</th>
									<td>Continuous Assessment</td>
									<td>30 Marks</td>
									<td>Class tests, assignments, quizzes, case studies & class participation.</td>
								</tr>
								<tr>
									<th scope="row">02</th>
									<td>Mid-term Examination</td>
									<td>30 Marks</td>
									<td>Held in the middle of the semester</td>
								</tr>
								<tr>
									<th scope="row">03</th>
									<td>Final Examination</td>
									<td>40 Marks</td>
									<td>Held at the end of the semester</td>
								</tr>
								<tr>
									<td colspan="2">TOTAL</td>
									<td colspan="2">100 Marks</td>
								</tr>
							</tbody>
						</table>
						<h6>Grading : </h6>

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th scope="col">Numerical Grade</th>
									<th scope="col">Letter Grade</th>
									<th scope="col">GP</th>
									<th scope="col">#</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td scope="row">80% and above</td>
									<td>A+</td>
									<td>(A plus)</td>
									<td>4.0</td>
								</tr>
								<tr>
									<td scope="row">75% to less than 80%</td>
									<td>A</td>
									<td>(A regular)</td>
									<td>3.75</td>
								</tr>
								<tr>
									<td scope="row">70% to less than 75%</td>
									<td>A-</td>
									<td>(A minus)</td>
									<td>3.50</td>
								</tr>
								<tr>
									<td>65% to less than 70%</td>
									<td>B+</td>
									<td>(B plus)</td>
									<td>3.25</td>
								</tr>
								<tr>
									<td>60% to less than 65% </td>
									<td>B</td>
									<td>(B regular) </td>
									<td>3.00</td>
								</tr>
								<tr>
									<td>55% to less than 60%</td>
									<td>B-</td>
									<td>(B minus)</td>
									<td>2.75</td>
								</tr>
								<tr>
									<td>50% to less than 55%</td>
									<td>C+</td>
									<td>(C plus)</td>
									<td>2.50</td>
								</tr>
								<tr>
									<td>45% to less than 50%</td>
									<td>C</td>
									<td>(C regular)</td>
									<td>2.25</td>
								</tr>
								<tr>
									<td>40% to less than 45%</td>
									<td>D</td>
									<td>D</td>
									<td>2.0</td>
								</tr>
								<tr>
									<td>Less than 40%</td>
									<td>F</td>
									<td>F</td>
									<td>0.0</td>
								</tr>

							</tbody>
						</table>

					</div>

					<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
						<h3>ACADEMIC REGULATIONS</h3>
						<h6>COURSE REGISTRATION :</h6>
						<p>All students are required to register for courses by filling in the prescribed ‘Registration Form ‘(available at the respective department) in each semester until they have fulfilled all graduation requirements. Besides, without registration credits will not be counted. The date and time of registration will be announced in the Academic Calendar by the respective department. It is expected that all students should complete their registration within the scheduled period.</p>
						<p>However, those who are unable to register within the scheduled time must apply to the Head/ Coordinator of the program of the department explain the reasons for delay. If the Head/ Coordinator of the program approves the application, then a late registration may be made within the deadline. It should be noted that course registration would not be accepted unless the student gets clearance from the accounts office.</p>
						<p>A late fee for late registration may be imposed (i.e. Tk. 500/- to Tk. 1,000/- as decided by the authority) after the prescribed schedule.</p>
						<p>If any student fails to complete the course registration within the deadline, then s/he shall be considered as unregistered for that particular semester and consequently s/he shall be barred from attending any classes or examinations.</p>
						<h6>CLASS ATTENDANCES :</h6>
						<p>Students are required to attend all lectures, tutorials, lab works etc. of the courses that they have registered. Normally 70% attendance is required of a student to be eligible to sit in the semester final examination.</p>
						<h6>SEMESTER LEAVE / LEAVE OF ABSENCE : </h6>
						<p>A student, who requires spending a period of time away from the university, may request for a leave of absence. In this regard, student must apply stating valid grounds to the Head of the department through the Batch Advisor of the department concerned.</p>
						<p>Permission for leave of absence/advance semester leave should be taken before the commencement of a new semester.</p>
						<p>The length of leave is maximum for two semesters.
						No fees will have to be paid by a student during such leave but his right to use university facilities is suspended while the leave is in effect.</p>
						<p>A student, who is suffering from a prolonged illness, may request for further period of leave of absence on medical grounds. To qualify for such an extension a student must submit a comprehensive medical report along with the application.</p>
						<p>If any undergraduate student is found absent/unregistered up to two semesters without having permission, s/he has to pay Tk. 1,000/- as fine for each semester dropped to continue the study at NUBT Khulna</p>
						<p>However, if any undergraduate or postgraduate student is found absent/unregistered without permission for more than two consecutive semesters, s/he will be considered as a discontinued student. If such student wishes to continue the study, an appeal may be made to the Registrar through Head of the department for re-admission. The competent authority of the university will take the decision for re-admission.</p>
						<h6>TRANSFER OF CREDITS :</h6>
						<p>Students of other recognized universities or any higher learning institutions, may apply for credit transfer at NUBT Khulna. Completed courses from said institutions should be similar & equivalent to the corresponding course(s) at NUBT Khulnaand only ‘B’ grade & above are accepted.</p>
						<p>The interested candidate may apply to the Head of the department along with the following documents :
							> Official Transcript (original) of the completed courses;
							> Detailed Syllabus of the completed courses;
						No Objection/Migration certificate from the previous institution.</p>
						<p>The Equivalence Committee of the respective department of NUBT Khulna will determine the equivalence of the courses applied for.</p>
						<p>The maximum credit may be allowed to transfer up to 50% of the total credits required for graduation of a particular program of study at NUBT Khulna.</p>
						<h6>POLICIES FOR FEES & CHARGES :</h6>
						<p>It is the responsibility of all students to be familiarized with the procedure regarding payment of fees and others of the university.</p>

						<p>All students are charged with the full fees and charges unless the university determines that a student pays the subsidized fees.</p>

						<p>Tuition and semester fee for each semester should be paid in full amount during the course registration of the semester or by three fixed installments.</p>
						<p>Dues (if any) should be settled at least two weeks before the final examination, otherwise students might be barred from sitting in the final examination. Moreover, results of their examination will be withheld until the fees are being settled.</p>
						<p>The university has the right to revise the fee structure without any prior notice.</p> 	 
						<h6>REFUND POLICIES :</h6>	
						<p>A student will be entitled to get 100% refund of paid amounts, if the university is unable to offer that particular program.</p>

						<p>A student will get 90% refund of fees paid excluding admission fee, if s/he submits an application to the Registrar for withdrawal from the program within one week of his/her admission</p>

						<p>If any student gets expulsion from the university due to fake information during admission, s/he will not get any refund of paid amounts at any stage.</p>
						<h6>COURSE WITHDRAWAL :</h6>
						<p>A registered student shall be entitled to get 50% refund only from tuition fees payable for the respective semester as follows:</p>

						<p>> S/he must apply within the first week of classes after mid-term examinations;
							> Application should be submitted to the Head of the department through the Course Teacher concerned;</p>

							<p>No application will be considered after the said period of time and consequently a student will liable to pay 100% payable amount of all charges for the semester.</p>
							<h6>SEMESTER DROP :</h6>

							<p>A registered student shall be entitled to get 50% refund only from tuition fees payable for the respective semester as follows :</p>
							<p>> S/he must apply within two weeks of the commencement of the semester;
								> Application should be submitted to the Head of the department through the Batch Adviser concerned;
							No application will be considered after the said period of time and consequently a student will liable to pay 100% payable amount of all charges for the semester.</p>

						</div>
						<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
							<h3>ACADEMIC CALENDAR</h3>
							<h6>Examination System</h6>
							<p>An academic year is divided into three semesters.<br>
								Each semester is of 16 weeks duration, which is distributed as follows:<br>
								14 weeks for instruction, 2-weeks for examination and break.<br>
							The schedules for three semesters are as follows.</p>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>Semester</th>
			<th>Activity</th>
			<th>Duration</th>
		</tr>
	</thead>
	<tbody>

		<?php
		foreach ($semesters as $key => $semester) {
			foreach ($semester['Activity'] as $key2 => $activity) {
				$count_act = count($semester['Activity']);
				?>
				<tr>
				<?php
				if($key2==0) echo '<td rowspan="'.$count_act.'"><br>'.$semester['Semester']['title'].'</td>';
				?>								
					<td><?=$semester['Activity'][$key2]['title']?></td>
					<td><?=$semester['Activity'][$key2]['duration']?></td>
				</tr>
				<?php
			}
		}
		?>
	</tbody>
</table>
</div>
<div class="tab-pane fade" id="list-faculties" role="tabpanel" aria-labelledby="list-faculties-list">
	<h3>SYLLABUS</h3>
	<h6>There are all programs syllabus which are approved by the UGC:</h6>

	<?php 

	foreach ($syllabuses as $key => $syllabus) {
		?>
		<a target="_blank" href="<?php echo $this->Html->url("/files/syllabuses/".$syllabus['Syllabus']['file'])?>"><p><?=$syllabus['Department']['departmentName']?></p></a>
	<?php
	}
	 ?>

	
	<!-- <p>02. B.Sc. in Electrical and Electronic Engineering (EEE) </p>
	<p>03. B.Sc. in Civil Engineering (CE)</p>
	<p>04. B.Sc. in Architecture (A. Arch)</p>
	<p>05. Bachelor of Business Administration (BBA)</p>
	<p>06. Master of Business Administration (MBA) </p>
	<p>07. Master of Business Administration (EMBA)</p>
	<p>08. Bachelor of Arts (Hons) in English Language & Literature </p>
	<p>09. Master of Arts in English (Preli. & Final)</p>
	<p>10. Bachelor of Arts (Hons.) in Bangla (4 Years)</p>
	<p>11. Bachelor of Social Science BSS (Hons) in Economics </p>
	<p>12. Master of Social Science (MSS) in Economics </p> -->
</div>
</div>
</div>
</div>
</div>
</div>
