<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>ACADEMIC CALENDAR</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-8 ">
				<ul class="list-inline pt-4 list-inline-custom float-right">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item"> / Academic  /</li>
					<li class="nav-item list-inline-item">Academic Calendar</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<h6>Examination System</h6>
		<p>An academic year is divided into three semesters.<br>
			Each semester is of 16 weeks duration, which is distributed as follows:<br>
			14 weeks for instruction, 2-weeks for examination and break.<br>
		The schedules for three semesters are as follows.</p>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Semester</th>
					<th>Activity</th>
					<th>Duration</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($semesters as $key => $semester) {
					foreach ($semester['Activity'] as $key2 => $activity) {
						$count_act = count($semester['Activity']);
				?>
				<tr>
					<?php
					if($key2==0) echo '<td rowspan="'.$count_act.'"><br>'.$semester['Semester']['title'].'</td>';
					?>
					<td><?=$semester['Activity'][$key2]['title']?></td>
					<td><?=$semester['Activity'][$key2]['duration']?></td>
				</tr>
				<?php
				}
				}
				?>
			</tbody>
		</table>
	</div>
</div>