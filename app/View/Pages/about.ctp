<div class="title-about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h2>ABOUT NUB AND OUR HISTORY</h2>
                <p>Northern University Khulna</p>
            </div>
            <div class="col-md-6 col-sm-12">
                <ul class="list-inline pt-4 float-right list-inline-custom">
                    <li class="nav-item list-inline-item"><a href="#">Home </a></li>
                    <li class="nav-item list-inline-item "> / About  /</li>
                    <li class="nav-item list-inline-item ">About NUB and our History</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="admission p-3 shadow overflow-hidden">
        <h3 class="">NUB ABOUT NUB AND OUR HISTORY</h3>
        <p>Keeping in step with the rapidly increasing demand for higher education, the Government of the People’s Republic
            of Bangladesh passed the Private Universities Act in 1992 which has been revised and replaced by Private
            University Act 2010. Following the1992 the Act, Bangladesh witnessed a sudden flux of private universities with
            the promise to provide higher education to students that will be at par with global standards.
        </p>
        <div class="knowledge">
            <h4>Knowledge for Innovation and Change.</h4>
            <p>Northern University Bangladesh.</p>
        </div>
        <p>In 2002, a group of eminent academics recognized this exponential increase in demand and established Northern
            University Bangladesh on 17 October in order to bring quality education within the reach of students with modest
            economic means. Sponsored and founded by International Business Agriculture & Technology (IBAT) Trust now known
            as NUB Trust (NUBT), a registered, non-political, non-profit voluntary organization, NUB made its foray in to
            the field of higher education and established itself as a center for excellence.
        </p>
        <p>Presently Northern University Bangladesh (NUB) has 5 Faculties situated within the Dhaka Metropolitan city with
            its Permanent Campus near Haji camp at Ashkona, Dakshin Khan. NUB has affiliations & accreditations with
            international institutions in its effort to provide education that can provide students with all the tools
            needed to face the challenges in any chosen career from any specialized field & the global industry. NUB has
            been authorized to confer degrees and certificates in all branches of knowledge including Business, Arts &
            Humanities, Science and Engineering, Law, Pharmacy and Public Health within the levels that includes Bachelors,
            Masters & is in the process of acquiring the level of M Phil & Ph. D. in collaboration with foreign
            universities.
        </p>
        <p>Northern University Bangladesh is now owned and managed by a group of academics under the illustrious and
            reputable banner of NUB Trust, who intend to not only provide higher education to the deserving students with
            moderate means but also aims to mould individuals with a specific skill set along with the value of social
            responsibility. Northern University Bangladesh believes in building a better future for the citizens of
            Bangladesh by enhancing its socio-economic empowerment.
        </p>
        <p>As a great & wise man had once said:
        </p>
        <div class="knowledge">
            <h4>“If we could change ourselves, the tendencies in the world would also change. As a man changes his own
            nature, so does the attitude of the world change towards him. … We need not wait to see what others do.”
            </h4>
            <span>
                <span>&nbsp;
                </span>Mahatma Gandhi
            </span>
        </div>
        <p>Northern University Bangladesh believes in providing Knowledge for Innovation & Change.
        </p>
    </div>
</div>