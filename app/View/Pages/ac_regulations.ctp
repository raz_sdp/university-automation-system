<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2>ACADEMIC REGULATIONS</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-6">
				<ul class="list-inline pt-4 list-inline-custom float-right">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item"> / Academic  /</li>
					<li class="nav-item list-inline-item">Academic Regulations</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<h6>COURSE REGISTRATION :</h6>
		<p>All students are required to register for courses by filling in the prescribed ‘Registration Form ‘(available at the respective department) in each semester until they have fulfilled all graduation requirements. Besides, without registration credits will not be counted. The date and time of registration will be announced in the Academic Calendar by the respective department. It is expected that all students should complete their registration within the scheduled period.</p>
		<p>However, those who are unable to register within the scheduled time must apply to the Head/ Coordinator of the program of the department explain the reasons for delay. If the Head/ Coordinator of the program approves the application, then a late registration may be made within the deadline. It should be noted that course registration would not be accepted unless the student gets clearance from the accounts office.</p>
		<p>A late fee for late registration may be imposed (i.e. Tk. 500/- to Tk. 1,000/- as decided by the authority) after the prescribed schedule.</p>
		<p>If any student fails to complete the course registration within the deadline, then s/he shall be considered as unregistered for that particular semester and consequently s/he shall be barred from attending any classes or examinations.</p>
		<h6>CLASS ATTENDANCES :</h6>
		<p>Students are required to attend all lectures, tutorials, lab works etc. of the courses that they have registered. Normally 70% attendance is required of a student to be eligible to sit in the semester final examination.</p>
		<h6>SEMESTER LEAVE / LEAVE OF ABSENCE : </h6>
		<p>A student, who requires spending a period of time away from the university, may request for a leave of absence. In this regard, student must apply stating valid grounds to the Head of the department through the Batch Advisor of the department concerned.</p>
		<p>Permission for leave of absence/advance semester leave should be taken before the commencement of a new semester.</p>
		<p>The length of leave is maximum for two semesters.
		No fees will have to be paid by a student during such leave but his right to use university facilities is suspended while the leave is in effect.</p>
		<p>A student, who is suffering from a prolonged illness, may request for further period of leave of absence on medical grounds. To qualify for such an extension a student must submit a comprehensive medical report along with the application.</p>
		<p>If any undergraduate student is found absent/unregistered up to two semesters without having permission, s/he has to pay Tk. 1,000/- as fine for each semester dropped to continue the study at NUBT Khulna</p>
		<p>However, if any undergraduate or postgraduate student is found absent/unregistered without permission for more than two consecutive semesters, s/he will be considered as a discontinued student. If such student wishes to continue the study, an appeal may be made to the Registrar through Head of the department for re-admission. The competent authority of the university will take the decision for re-admission.</p>
		<h6>TRANSFER OF CREDITS :</h6>
		<p>Students of other recognized universities or any higher learning institutions, may apply for credit transfer at NUBT Khulna. Completed courses from said institutions should be similar & equivalent to the corresponding course(s) at NUBT Khulnaand only ‘B’ grade & above are accepted.</p>
		<p>The interested candidate may apply to the Head of the department along with the following documents :
			> Official Transcript (original) of the completed courses;
			> Detailed Syllabus of the completed courses;
		No Objection/Migration certificate from the previous institution.</p>
		<p>The Equivalence Committee of the respective department of NUBT Khulna will determine the equivalence of the courses applied for.</p>
		<p>The maximum credit may be allowed to transfer up to 50% of the total credits required for graduation of a particular program of study at NUBT Khulna.</p>
		<h6>POLICIES FOR FEES & CHARGES :</h6>
		<p>It is the responsibility of all students to be familiarized with the procedure regarding payment of fees and others of the university.</p>
		<p>All students are charged with the full fees and charges unless the university determines that a student pays the subsidized fees.</p>
		<p>Tuition and semester fee for each semester should be paid in full amount during the course registration of the semester or by three fixed installments.</p>
		<p>Dues (if any) should be settled at least two weeks before the final examination, otherwise students might be barred from sitting in the final examination. Moreover, results of their examination will be withheld until the fees are being settled.</p>
		<p>The university has the right to revise the fee structure without any prior notice.</p>
		<h6>REFUND POLICIES :</h6>
		<p>A student will be entitled to get 100% refund of paid amounts, if the university is unable to offer that particular program.</p>
		<p>A student will get 90% refund of fees paid excluding admission fee, if s/he submits an application to the Registrar for withdrawal from the program within one week of his/her admission</p>
		<p>If any student gets expulsion from the university due to fake information during admission, s/he will not get any refund of paid amounts at any stage.</p>
		<h6>COURSE WITHDRAWAL :</h6>
		<p>A registered student shall be entitled to get 50% refund only from tuition fees payable for the respective semester as follows:</p>
		<p>> S/he must apply within the first week of classes after mid-term examinations;
			> Application should be submitted to the Head of the department through the Course Teacher concerned;</p>
			<p>No application will be considered after the said period of time and consequently a student will liable to pay 100% payable amount of all charges for the semester.</p>
			<h6>SEMESTER DROP :</h6>
			<p>A registered student shall be entitled to get 50% refund only from tuition fees payable for the respective semester as follows :</p>
			<p>> S/he must apply within two weeks of the commencement of the semester;
				> Application should be submitted to the Head of the department through the Batch Adviser concerned;
			No application will be considered after the said period of time and consequently a student will liable to pay 100% payable amount of all charges for the semester.</p>
		</div>
	</div>