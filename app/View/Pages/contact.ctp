<section id="" class="contact">
    <div class="contact-banner">
        <div class="container">
            <h1>Contact Us</h1>
        </div>
    </div>
    <div class="container">
        <div class="row text-center pt-5 pb-5  glance">
            <div class="col-md-3">
                <i class="fas fa-map-marker-alt size7x pb-3"></i>
                <p><?=$gen_sett['Generalsetting']['location']?></p>
            </div>
            <div class="col-md-3">
                <i class="fas fa-phone size7x pb-3"></i>
                <p><?=$gen_sett['Generalsetting']['telephone']?><br>
                <?=$gen_sett['Generalsetting']['phone']?></p>
            </div>
            <div class="col-md-3">
                <i class="fas fa-envelope size7x pb-3"></i>
                <p><?=$gen_sett['Generalsetting']['mail']?></p>
            </div>
            <div class="col-md-3">
                <i class="far fa-clock size7x pb-3"></i>
                <p>Sat - Thu : 09:00 AM to 06:00 PM<br>
                Friday : 09:00 AM to 06:00 PM</p>
            </div>
        </div>
        <div class="row  pt-5 pb-5">
            <div class="col-md-5">
                <h4 class="font-weight-bold  pt-3 pb-3">GET IN TOUCH WITH US</h4>
                <p><i class="icon-color pr-2 fas fa-map-marker-alt"></i> <?=$gen_sett['Generalsetting']['location']?></p>
                <p><i class="icon-color pr-2 fas fa-phone"></i> Tel: <?=$gen_sett['Generalsetting']['telephone']?></p>
                <p><i class="icon-color pr-2 fas fa-phone"></i> Phone: <?=$gen_sett['Generalsetting']['phone']?></p>
                <p><i class="icon-color pr-2 fas fa-envelope"></i> Email: <?=$gen_sett['Generalsetting']['mail']?></p>
            </div>
            <div class="col-md-7">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="inputEmail4" placeholder="Your Name">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="password" class="form-control" id="inputPassword4" placeholder="Phone">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="password" class="form-control" id="inputPassword4" placeholder="subject">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea rows="7" type="text" class="form-control" id="inputAddress" placeholder="Massage.."></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-news">SEND MASSAGE</button>
                </form>
            </div>
        </div>
    </div>
</section>