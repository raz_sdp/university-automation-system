<section>
	<div class="">
		<img src="img/dept-pro-ban.jpg" class="img-fluid">
		<div class="container">
			<div class="row profile-contant text-white">
				<div class="col-md-4 text-right bg-primary p-4">
					<h4>Md. Minhaj Uddin Monir</h4>
					<p class="mb-0"><i>Assistant Professor</i></p>
					<p>Department of Petroleum and Mining Engineering Jashore University of Science and Technolog
					</p>
				</div>
				<div class="col-md-4 bg-success text-center order"  >
					<div class="profile-img">
						<img src="img/images6.jpg"><br>
						<a href="#"><i class="fab fa-facebook-square"></i></a>
						<a href="#"><i class="fab fa-facebook-square"></i></a>
					</div>
				</div>
				<div class="col-md-4 bg-info p-4">
					<h4>Contact Info</h4>
					<p><i class="fas fa-map-marker-alt"></i> Bangabandhu Sheikh Mujib Academic Building, Floor: 7</p>
					<p><i class="fas fa-envelope-open"></i> monir_pme@just.edu.bd</p>
					<p><i class="fas fa-mobile"></i> +8801717713859</p>
				</div>
			</div>
		</div>
	</div>
</section>