<section>
    <div class="title-about">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2>ABOUT NUB
                    </h2>
                    <p>Northern University Bangladesh
                    </p>
                </div>
                <div class="col-md-8 ">
                    <ul class="list-inline float-right pt-4 list-inline-custom">
                        <li class="nav-item list-inline-item">
                            <a href="#">Home
                            </a>
                        </li>
                        <li class="nav-item list-inline-item"> / about NUB /
                        </li>
                        <li class="nav-item list-inline-item ">About Nub And Our History
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="about-right">
        <div class="container p-5">
            <div class="row">
                <div class="col-md-4" data-aos="fade-right">
                    <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action custom-left active" id="list-home-list" data-toggle="list"
                        href="#list-home" role="tab" aria-controls="home">About NUB And Our History
                        <span class="float-right">
                            <i class="fas fa-arrow-right">
                            </i>
                        </span>
                    </a>
                    <a class="list-group-item list-group-item-action custom-left" id="list-profile-list" data-toggle="list"
                    href="#list-profile" role="tab" aria-controls="profile">Vision And Mission
                    <span class="float-right">
                        <i class="fas fa-arrow-right">
                        </i>
                    </span>
                </a>
                <a class="list-group-item list-group-item-action custom-left" id="list-messages-list" data-toggle="list"
                href="#list-messages" role="tab" aria-controls="messages">Message From The Chairman
                <span class="float-right">
                    <i class="fas fa-arrow-right">
                    </i>
                </span>
            </a>
            <a class="list-group-item list-group-item-action custom-left" id="list-settings-list" data-toggle="list"
            href="#list-settings" role="tab" aria-controls="settings">Message From VC
            <span class="float-right">
                <i class="fas fa-arrow-right">
                </i>
            </span>
        </a>
        <a class="list-group-item list-group-item-action custom-left" id="list-faculties-list" data-toggle="list"
        href="#list-faculties" role="tab" aria-controls="faculties">Faculties
        <span class="float-right">
            <i class="fas fa-arrow-right">
            </i>
        </span>
    </a>
</div>
</div>
<div class="col-md-8" data-aos="fade-left">
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
            <h3 class="">NUB ABOUT NUB AND OUR HISTORY</h3>
            <p>Keeping in step with the rapidly increasing demand for higher education, the Government of the People’s Republic
                of Bangladesh passed the Private Universities Act in 1992 which has been revised and replaced by Private
                University Act 2010. Following the1992 the Act, Bangladesh witnessed a sudden flux of private universities with
                the promise to provide higher education to students that will be at par with global standards.
            </p>
            <div class="knowledge">
                <h4>Knowledge for Innovation and Change.</h4>
                <p>Northern University Bangladesh.</p>
            </div>
            <p>In 2002, a group of eminent academics recognized this exponential increase in demand and established Northern
                University Bangladesh on 17 October in order to bring quality education within the reach of students with modest
                economic means. Sponsored and founded by International Business Agriculture & Technology (IBAT) Trust now known
                as NUB Trust (NUBT), a registered, non-political, non-profit voluntary organization, NUB made its foray in to
                the field of higher education and established itself as a center for excellence.
            </p>
            <p>Presently Northern University Bangladesh (NUB) has 5 Faculties situated within the Dhaka Metropolitan city with
                its Permanent Campus near Haji camp at Ashkona, Dakshin Khan. NUB has affiliations & accreditations with
                international institutions in its effort to provide education that can provide students with all the tools
                needed to face the challenges in any chosen career from any specialized field & the global industry. NUB has
                been authorized to confer degrees and certificates in all branches of knowledge including Business, Arts &
                Humanities, Science and Engineering, Law, Pharmacy and Public Health within the levels that includes Bachelors,
                Masters & is in the process of acquiring the level of M Phil & Ph. D. in collaboration with foreign
                universities.
            </p>
            <p>Northern University Bangladesh is now owned and managed by a group of academics under the illustrious and
                reputable banner of NUB Trust, who intend to not only provide higher education to the deserving students with
                moderate means but also aims to mould individuals with a specific skill set along with the value of social
                responsibility. Northern University Bangladesh believes in building a better future for the citizens of
                Bangladesh by enhancing its socio-economic empowerment.
            </p>
            <p>As a great & wise man had once said:
            </p>
            <div class="knowledge">
                <h4>“If we could change ourselves, the tendencies in the world would also change. As a man changes his own
                    nature, so does the attitude of the world change towards him. … We need not wait to see what others do.”
                </h4>
                <span>
                    <span>&nbsp;
                    </span>Mahatma Gandhi
                </span>
            </div>
            <p>Northern University Bangladesh believes in providing Knowledge for Innovation & Change.
            </p>
        </div>
        <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
            <h3>VISION AND MISSION
            </h3>
            <h6>VISION :
            </h6>
            <p>Vision of NUB is to take part in the collective efforts to enhance socio-economic development in the region by
                offering opportunities to obtain knowledge and skills essential for better living in the new century. Our vision
                tags as 'Knowledge for Innovation and Change'.
            </p>
            <h6>MISSION :
            </h6>
            <p>Mission of NUB is to offer such programs of study and related functions that will be directly linked to
                socio-economic empowerment of the people of the country. It will also conduct educational research and
                developmental programs of higher quality that would be at par with industry needs, skill-ready world and real
                life situation.
            </p>
        </div>
        <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
            <h3>MESSAGE FROM THE CHAIRMAN
            </h3>
            <div class="" style="float: left; margin: 2px 10px; width: 192px;">
                <img src="img/principal.png" alt="principal" class="img-fluid img-thumbnail">
            </div>
            <p><?=$gen_sett['Generalsetting']['welcomeMsg']?>
            </p>
        </div>
        <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
            <h3>MESSAGE FROM VC
            </h3>
            <div style="float: left; margin: 2px 10px; width: 192px;">
                <img src="img/principal.png" alt="principal" class="img-fluid img-thumbnail">
            </div>
            <p>As you are aware of the fact that the vision of Northern University Bangladesh(NUB) is to create knowledge for
                the socio-economic development of the people of the region and their overall empowerment through application
                oriented disciplines to raise their skill; all our efforts in NUB is focused to those goals and our strategies
                are geared to that. At NUB we seek through a seamless effort to nurture and develop the young as well as matured
                minds with a mindset that constantly innovates and creates new ideas in all the braches of higher education.
            </p>
            <p>Our pedagogy is modern as well as student friendly, our class room facilities are of international standard and
                our faculty members are world class. In this world of cut throat competition and cutting edge technology we have
                taken the challenge to groom our students with the skill which they can use from the day one as they enter into
                the job market or build their own enterprise.
            </p>
            <p>In line with the goal of the university to create, disseminate and store new knowledge, we emphasize on teaching,
                undertaking research and regular publication in our university. All our campuses are well equipped with
                technology driven teaching aids, and all our scientific labs are providing excellent research opportunities both
                for students and the faculty members. Equal research facilities are also available for the business school and
                social science students and faculty members. We also regularly publish journals from all the faculties of the
                universities which are blind refereed. We patronize with great care case research for the business and the law
                students.
            </p>
            <p>We are compliant to government and UGC rules and our permanent campus is now being built with all the facilities
                of a modern campus and we hope to move there within a foreseeable future. It may be mentioned here that part of
                our permanent campus is already operational with some of the departments of science faculty holding their
                classes regularly in that campus.
            </p>
            <p>The economy of Bangladesh is growing steadily and the private sector is now hyper active due to supportive
                government policies . We now require regular flow of young and skilled graduates to run the enterprises coming
                up in the country. NUB is working relentlessly to provide this service for our economy.
            </p>
            <p> We believe on the basic values of the society and the spirit of our glorious liberation war and remember with
                gratitude the sacrifices of our martyrs and the valiant freedom struggle of our people under the legendary
                leadership of our father of the nation Bangabandhu Shaikh Mujibur Rahman. We want our graduates to reflect these
                values and be the standard bearer of their almamater in whatever capacity they are serving in future.
            </p>
            <p>Prof. Dr. Anwar Hossain
                <br>
                Vice-Chancellor
                <br>
                Northern University Bangladesh
            </p>
        </div>
        <div class="tab-pane fade" id="list-faculties" role="tabpanel" aria-labelledby="list-faculties-list"
        data-aos="fade-left">
        <h3>FACULTIES
        </h3>
        <p>The university is shaped by the unique resources available through its location in Dhaka, the capital city of
            Bangladesh . NUB Permanent Campus is situated at 111/2 Kawla Jamea Mosjid Road, Dakshin Khan (near international
            airport), Uttra, Dhaka. The registered office of Northern university Bangladesh are at follows:
            <div class="choose-accrodin">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php
                    foreach($faculties as $key => $faculty) {
                                        #pr($faculty);die;
                        ?>
                        <!-- START ACCORDION DESIGN SECTION  -->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading<?=$key?>">
                                <h4 class="panel-title" data-aos="fade-right">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$key?>" aria-expanded="false"
                                        aria-controls="collapse<?=$key?>" class="collapsed">
                                        <?=$faculty['Faculty']['facultyName']?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?=$key?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$key?>"
                                aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="card  mt-4 mb-4 shadow">
                                        <ul class="list-group list-group-flush list-custom">
                                            <li class="list-group-item">
                                                <strong>Dean
                                                </strong>
                                                <h6><?=$faculty['Faculty']['deanName']?>
                                                </h6>
                                                <p>Departments:
                                                </p>
                                            </li>
                                            <?php
                                            foreach($faculty['Department'] as $dept){
                                                            #pr($dept);
                                                ?>
                                                <a href="<?php echo $this->html->url('/dept')?>">
                                                    <li class="list-group-item">
                                                        <i class="fas fa-link">
                                                        </i> <?=$dept['departmentName']?>
                                                    </li>
                                                </a>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END ACCORDION DESIGN SECTION  -->
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</section>