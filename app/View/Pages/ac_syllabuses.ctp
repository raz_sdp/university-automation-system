<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2>ACADEMIC SYLLABUSES</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-6 col-sm-12">
				<ul class="list-inline pt-4 list-inline-custom float-right">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item"> / Academic  /</li>
					<li class="nav-item list-inline-item">Academic syllabuses</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<h3>SYLLABUS</h3>
		<h6>There are all programs syllabus which are approved by the UGC:</h6>

		<?php 

		foreach ($syllabuses as $key => $syllabus) {
			?>
			<a target="_blank" href="<?php echo $this->Html->url("/files/syllabuses/".$syllabus['Syllabus']['file'])?>"><p><?=$syllabus['Department']['departmentName']?></p></a>
			<?php
		}
		?>
	</div>
</div>