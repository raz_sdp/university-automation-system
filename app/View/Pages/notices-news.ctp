<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2>NOTICES & NEWS</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-8 ">
				<ul class="list-inline pt-4">
					<li class="nav-item list-inline-item float-right ">All News</li>
					<li class="nav-item list-inline-item float-right "> / Notices & News  /</li>
					<li class="nav-item list-inline-item float-right"><a href="#">Home </a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="about-right">
	<div class="container p-5">
		<div class="row">
			<div class="col-md-4">
				<div class="list-group" id="list-tab" role="tablist">
					<a class="list-group-item list-group-item-action custom-left active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">All Notices<span class="float-right"><i class="fas fa-arrow-right"></i></span></a>

					<a class="list-group-item list-group-item-action custom-left" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Tender Notices<span class="float-right"><i class="fas fa-arrow-right"></i></span>
					</a>

					<a class="list-group-item list-group-item-action custom-left" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Job Circular <span class="float-right"><i class="fas fa-arrow-right"></i></span></a>

					<a class="list-group-item list-group-item-action custom-left" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">All News & Events<span class="float-right"><i class="fas fa-arrow-right"></i></span>
					</a>
				</div>
			</div>
			<div class="col-md-8">
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
						<h3 class="">ALL PUBLISHED NOTICES</h3>
						<div class="undergraduate border p-3 shadow overflow-hidden">

							<?php 
							foreach ($notices as $key => $notice) {
								?>
								<div class="undergraduate-sub">
									<p class="pt-3"><?=$notice['Notice']['title']?></p>
									<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
									</p>
									<?php
									if($key<count($notices)-1) echo '<div class="border"></div>';
									?>						
								</div>
								<?php
							}
							?>
							<div class="float-right pt-5" aria-label="Page navigation example">
								<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
						<h3>TENDER NOTICES</h3>
						<div class="undergraduate border p-3 shadow overflow-hidden">
							<?php
							foreach ($notices as $key => $notice) {

								if($notice['Notice']['type'] == 'Tender Notice'){
									?>
									<div class="undergraduate-sub">
										<p class="pt-3"><?=$notice['Notice']['title']?></p>
										<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
										</p>
										<?php
										if($key<count($notices)-1) echo '<div class="border"></div>';
										?>
									</div>
									<?php
								}
							}
							?>
						</div>
					</div>

					<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
						<h3>JOB CIRCULAR</h3>
						<div class="undergraduate border p-3 shadow overflow-hidden">
							<?php 
							foreach ($notices as $key => $notice) {

								if($notice['Notice']['type'] == 'Job Circular'){
									?>
									<div class="undergraduate-sub">
										<p class="pt-3"><?=$notice['Notice']['title']?></p>
										<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
										</p>
										<?php
										if($key<count($notices)-1) echo '<div class="border"></div>';
										?>						
									</div>
									<?php
								}
							}
							?>

							<div class="float-right pt-5" aria-label="Page navigation example">
								<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
						<h3>ALL NEWS & EVENTS</h3>

						<div class="row card-group">
							<?php 
							foreach ($news as $key => $sin_news) {
								?>
								<div class="col-md-6 ">
									<div class="card mb-4">
										<img src="<?php echo $this->Html->url('/files/news/'.$sin_news['News']['img'])?>" class="card-img-top" alt="...">
										<div class="card-body">
											<p class="card-text"><?=$sin_news['News']['title']?></p>
										</div>
										<div class="card-footer">
											<small class="text-muted"><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($sin_news['News']['created']), 'd M Y'); ?></small>
										</div>
									</div>
								</div>
								<?php
							}
							?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
