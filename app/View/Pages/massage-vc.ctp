<section>
	<div>
		<img src="img/member-bg.jpg" alt="principal" class="img-fluid" width="100%">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-3 prin-img">
				<img src="img/principal.png" alt="principal" class="img-fluid">
			</div>
			<div class="col-md-9 tab-pane pt-4">
				<h3>MESSAGE FROM VC</h3>
				<p>As you are aware of the fact that the vision of Northern University Bangladesh(NUB) is to create knowledge for
					the socio-economic development of the people of the region and their overall empowerment through application
					oriented disciplines to raise their skill; all our efforts in NUB is focused to those goals and our strategies
					are geared to that. At NUB we seek through a seamless effort to nurture and develop the young as well as matured
					minds with a mindset that constantly innovates and creates new ideas in all the braches of higher education.
				</p>
				<p>Our pedagogy is modern as well as student friendly, our class room facilities are of international standard and
					our faculty members are world class. In this world of cut throat competition and cutting edge technology we have
					taken the challenge to groom our students with the skill which they can use from the day one as they enter into
					the job market or build their own enterprise.
				</p>
				<p>In line with the goal of the university to create, disseminate and store new knowledge, we emphasize on teaching,
					undertaking research and regular publication in our university. All our campuses are well equipped with
					technology driven teaching aids, and all our scientific labs are providing excellent research opportunities both
					for students and the faculty members. Equal research facilities are also available for the business school and
					social science students and faculty members. We also regularly publish journals from all the faculties of the
					universities which are blind refereed. We patronize with great care case research for the business and the law
					students.
				</p>
				<p>We are compliant to government and UGC rules and our permanent campus is now being built with all the facilities
					of a modern campus and we hope to move there within a foreseeable future. It may be mentioned here that part of
					our permanent campus is already operational with some of the departments of science faculty holding their
					classes regularly in that campus.
				</p>
				<p>The economy of Bangladesh is growing steadily and the private sector is now hyper active due to supportive
					government policies . We now require regular flow of young and skilled graduates to run the enterprises coming
					up in the country. NUB is working relentlessly to provide this service for our economy.
				</p>
				<p> We believe on the basic values of the society and the spirit of our glorious liberation war and remember with
					gratitude the sacrifices of our martyrs and the valiant freedom struggle of our people under the legendary
					leadership of our father of the nation Bangabandhu Shaikh Mujibur Rahman. We want our graduates to reflect these
					values and be the standard bearer of their almamater in whatever capacity they are serving in future.
				</p>
				<p>Prof. Dr. Anwar Hossain
					<br>
					Vice-Chancellor
					<br>
					Northern University Bangladesh
				</p>
			</div>
		</div>
	</div>
</section>