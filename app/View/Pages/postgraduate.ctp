<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2><b>POSTGRADUATE</b> ADMISSION</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-6 col-sm-12">
				<ul class="list-inline pt-4 float-right list-inline-custom">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item "> / Admission  /</li>
					<li class="nav-item list-inline-item ">Postgraduate</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<?php
		foreach ($notices as $key => $notice) {
			if($notice['Notice']['type'] == 'Postgraduate Admission'){
		?>
		<div class="undergraduate-sub">
			<p class="pt-3"><?=$notice['Notice']['title']?></p>
			<p class="notice-pra "><i class="fas fa-calendar-alt notice-pra"></i> <?=date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?><span class="pl-2"><a class="notice-span" href="<?php echo $this->Html->url("/files/notices/".$notice['Notice']['filename'])?>"><i class="fas fa-download"></i> Download</a></span>
		</p>
		<?php
		if($key<count($notices)-1) echo '<div class="border"></div>';
		?>
	</div>
	<?php
	}
	}
	?>
	<ul class="pagination float-right">
		<li class="page-item"><a class="page-link" href="#">Previous</a></li>
		<li class="page-item"><a class="page-link" href="#">1</a></li>
		<li class="page-item"><a class="page-link" href="#">2</a></li>
		<li class="page-item"><a class="page-link" href="#">Next</a></li>
	</ul>
</div>
</div>