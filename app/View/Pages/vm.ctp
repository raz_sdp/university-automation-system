<div class="title-about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h2>VISION AND MISSION</h2>
                <p>Northern University Khulna</p>
            </div>
            <div class="col-md-6 col-sm-12">
                <ul class="list-inline pt-4 float-right list-inline-custom">
                    <li class="nav-item list-inline-item"><a href="#">Home </a></li>
                    <li class="nav-item list-inline-item "> / Admission  /</li>
                    <li class="nav-item list-inline-item ">Postgraduate</li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="admission p-3 shadow overflow-hidden">
        <h3>VISION AND MISSION</h3>
        <h6>VISION :</h6>
        <p>Vision of NUB is to take part in the collective efforts to enhance socio-economic development in the region by
            offering opportunities to obtain knowledge and skills essential for better living in the new century. Our vision
            tags as 'Knowledge for Innovation and Change'.
        </p>
        <h6>MISSION :</h6>
        <p>Mission of NUB is to offer such programs of study and related functions that will be directly linked to
            socio-economic empowerment of the people of the country. It will also conduct educational research and
            developmental programs of higher quality that would be at par with industry needs, skill-ready world and real
            life situation.
        </p>
    </div>
</div>