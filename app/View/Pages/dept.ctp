<section class="">
	<div class="dep-banner-title">
		<div class="container dep-title">
			<h2>DEPARTMENT OF</h2>
			<h1 class="font-weight-bold">PETROLEUM AND MINING ENGINEERING</h1>
		</div>
	</div>
	<div class="container pt-5">
		<div class="row text-justify">
			<div class="col-md-6">
				<h2>MESSAGE FROM <b>CHAIRMAN</b></h2>
				<p>It is my great pleasure to invite you to the official website of the Department of Physics of Jessore University of Science and Technology (JUST). There is no denying the fact that the contributions of science and technology, more specifically, the contribution of Physics to develop a nation is beyond description. No nation can develop properly without the assistance of science and technology. To meet up the diversified demand of people, Physics has been playing the key role from the ‘Big Bang’ and it will continue till the destruction of the universe. Besides this, science and technology can’t be imagined without Physics and physical sciences. As we know, a university is the most suitable place for education and research in Physics, so the Department of Physics of this university is playing a vital role to produce qualitative Physicists for the development of the country as well as to fulfill the global needs. It is mentionable that the Department of Physics of Jessore University of Science and Technology started its journey in 2013 with a view to imparting higher study in Physics in Bangladesh.</p>
				<p><b>Md. Masum BiIllah</b></p>
				<p>Chairman (Current Charge</p>
			</div>
			<div class="col-md-6"></div>
			<div class="offset-md-6"></div>
			<div class="col-md-6">
				<h2>DEPARTMENT'S <b>VIEW</b></h2>
				<p><b>Introduction</b></p>
				<p>Department of Physics established and launched its journey in 2013. The first batch student enrollment was in 2013-2014 session. Currently, the degrees offered under this department are B. Sc. (Hons.) in Physics and M. Sc. (Thesis and Non-thesis group) in Physics. These programs produce skilled professionals to meet the gradually growing global demands of Physics experts through which the students can get a broad opportunity in research and job sections. In addition, these programs aim at training-up new generations to be scientists and researchers who will take leadership position in teaching, research and invention-based professions in home and abroad.</p>
				<p><b>Laboratory Facilities</b></p>
				<p>The Department of Physics has the sufficient laboratory facilities in fundamental and advanced level. The department has already established Mechanics Lab, Properties of Matter Lab, Electricity and Magnetism Lab, Vibrations and Waves Lab, Optics Lab, Electronics Lab, Computational Physics Lab, Atomic and Molecular Physics Lab, Solid State Physics Lab and Nuclear Physics Lab.</p>
				<p><b>Graduate Career Options</b></p>
				<p>The employment opportunities and possibilities for the Physics graduates are as numerous as they are varied. Physicists are employed in all scientific fields in home and abroad related to their knowledge in various fields of Physics. In home, there are a lot of specialized fields for the Physics graduates to build-up their career, e.g., Bangladesh Atomic Energy Commission (BAEC), Bangladesh Atomic Energy Regulatory Authority (BAERA), Nuclear Power Company of Bangladesh (NPCB), Bangladesh Council of Scientific and Industrial Research (BCSIR), Bangladesh Space Research and Remote Sensing Organization (SPARRSO), Bangladesh Power Development Board (BPDB), Bangladesh Meteorological Department (BMD), etc. may be mentioned herewith. In abroad, the graduates of Physics have the opportunities to be appointed as the scientists of various grades and researchers in various scientific institutes all over the world, including National Aeronautics and Space Administration (NASA), European Council for Nuclear Research (CERN), The Abdus Salam International Centre for Theoretical Physics (ICTP), The Max Planck Institute, etc.</p>
			</div>
		</div>
	</div>
	<div class="container">
		<h2 class="text-center pb-5 pt-5">LABORATORY <strong>FACILITIES</strong></h2>
		<div class="lab-item">
			<div class="row">
				<div class="col-md-5 pr-md-0">
					<div class="d-intro__img">
						<img src="/newsDefault.jpg" alt="" class="d-intro__img__val">
					</div>
				</div>
				<div class="col-md-2 text-center d-none d-md-block">
					<div class="d-vline"></div>
					<div class="d-circle"></div>
				</div>
				<div class="col-md-5 pl-md-0 d-flex justify-content-center justify-content-md-start align-items-center  mb-md-0 ">
					<div class="text-center text-md-left">
						<h1 class="mb-3"><b>ELECTRICITY & MAGNETISM LABORATORY</b></h1><p class="vcontent"></p>
					</div>
				</div>
			</div>
		</div>
		<div class="lab-item">
			<div class="row">
				<div class="col-md-5 pl-md-0 d-flex justify-content-center justify-content-md-start align-items-center  mb-md-0 ">
					<div class="text-center text-md-left">
						<h1 class="mb-3"><b>ELECTRONICS LABORATORY</b></h1><p class="vcontent"></p>
					</div>
				</div>
				<div class="col-md-2 text-center d-none d-md-block">
					<div class="d-vline"></div>
					<div class="d-circle"></div>
				</div>
				<div class="col-md-5 pr-md-0">
					<div class="d-intro__img">
						<img src="/newsDefault.jpg" alt="" class="d-intro__img__val">
					</div>
				</div>
			</div>
		</div>
		<div class="lab-item">
			<div class="row">
				<div class="col-md-5 pr-md-0">
					<div class="d-intro__img">
						<img src="/newsDefault.jpg" alt="" class="d-intro__img__val">
					</div>
				</div>
				<div class="col-md-2 text-center d-none d-md-block">
					<div class="d-vline"></div>
					<div class="d-circle"></div>
				</div>
				<div class="col-md-5 pl-md-0 d-flex justify-content-center justify-content-md-start align-items-center  mb-md-0 ">
					<div class="text-center text-md-left">
						<h1 class="mb-3"><b>OPTICS LABORATORY</b></h1><p class="vcontent"></p>
					</div>
				</div>
			</div>
		</div>
		<div class="lab-item">
			<div class="row">
				<div class="col-md-5 pl-md-0 d-flex justify-content-center justify-content-md-start align-items-center  mb-md-0 ">
					<div class="text-center text-md-left">
						<h1 class="mb-3"><b>SOLID STATE PHYSICS LABORATORY</b></h1><p class="vcontent"></p>
					</div>
				</div>
				<div class="col-md-2 text-center d-none d-md-block">
					<div class="d-vline"></div>
					<div class="d-circle"></div>
				</div>
				<div class="col-md-5 pr-md-0">
					<div class="d-intro__img">
						<img src="/newsDefault.jpg" alt="" class="d-intro__img__val">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div  class="faculty-members">
		<div class="container">
			<h2 class="text-center pb-5">OUR <strong>FACULTY</strong> MEMBERS</h2>
			<div class="row">
				<div class="col-md-6">
					<div class="t-box">
						<div class="t-box-img d-inline-block">
							<img src="img/member.jpg" alt="member" class="rounded-circle img-fluid">
						</div>
						<div class="t-box-des d-inline-block">
							<h4>MD AL AMIN</h4>
							<p class="m-0">Lecturer</p>
							<p class="m-0">example@gmail.com</p>
							<a href="<?php echo $this->html->url('/dept-profile')?>" class="btn sbtn">view</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="t-box">
						<div class="t-box-img d-inline-block">
							<img src="img/images1.png" alt="member" class="rounded-circle img-fluid">
						</div>
						<div class="t-box-des d-inline-block">
							<h4>MD AL AMIN</h4>
							<p class="m-0">Lecturer</p>
							<p class="m-0">example@gmail.com</p>
							<a href="#" class="btn sbtn">view</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="t-box">
						<div class="t-box-img d-inline-block">
							<img src="img/images4.jpg" alt="member" class="rounded-circle img-fluid">
						</div>
						<div class="t-box-des d-inline-block">
							<h4>MD AL AMIN</h4>
							<p class="m-0">Lecturer</p>
							<p class="m-0">example@gmail.com</p>
							<a href="#" class="btn sbtn">view</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="t-box">
						<div class="t-box-img d-inline-block">
							<img src="img/images3.jpg" alt="member" class="rounded-circle img-fluid">
						</div>
						<div class="t-box-des d-inline-block">
							<h4>MD AL AMIN</h4>
							<p class="m-0">Lecturer</p>
							<p class="m-0">example@gmail.com</p>
							<a href="#" class="btn sbtn">view</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>