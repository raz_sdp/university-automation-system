<div class="title-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2>ACADEMIC PROGRAMS</h2>
				<p>Northern University Khulna</p>
			</div>
			<div class="col-md-6 col-sm-12">
				<ul class="list-inline pt-4 list-inline-custom float-right">
					<li class="nav-item list-inline-item"><a href="#">Home </a></li>
					<li class="nav-item list-inline-item"> / Academic  /</li>
					<li class="nav-item list-inline-item">Academic Programs</li>					
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="admission p-3 shadow overflow-hidden">
		<h3 class="">ACADEMIC PROGRAMS</h3>
		<p>NUB has been expanding its academic programs at the Bachelors and Masters levels under different faculties and departments . At present, the university’s academic programs are as follows :</p>

		<div class="row">
			<?php 
			foreach ($faculties as $key => $faculty) {
				?>
				<div class="col-md-4 ">
					<div class="academic-program">
						<img src="<?php echo $this->html->url('/files/faculties/'.$faculty['Faculty']['facultyImage'])?>" alt="program" class="img-fluid img-thumbnail">
						<!-- <img src="img/program.jpg" alt="program" class="img-fluid img-thumbnail"> -->
						<h6><?=$faculty['Faculty']['facultyName']?></h6>
						<div class="">
							<ul>
								<?php 
								foreach ($faculty['Department'] as $dept) {
									?>
									<li><a href="<?php echo $this ->html->url('/dept')?>"><?=$dept['departmentName']?></a></li>
									<?php
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</div>