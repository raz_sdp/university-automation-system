<section class="tables">
    <div class="container-fluid section-top">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Notices</h3>
                    </div>
                    <div class="card-body">
                        <div class="news view p-5">
							<h2><?php echo __('News'); ?></h2>
								<dl class = "dl-horizontal">
									<dt><?php echo __('Id'); ?></dt>
									<dd>
										<?php echo h($news['News']['id']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Title'); ?></dt>
									<dd>
										<?php echo h($news['News']['title']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Description'); ?></dt>
									<dd>
										<?php echo h($news['News']['description']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Created'); ?></dt>
									<dd>
										<?php echo h($news['News']['created']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Modified'); ?></dt>
									<dd>
										<?php echo h($news['News']['modified']); ?>
										&nbsp;
									</dd>
								</dl>
							</div>
							<div class="actions">
								<h3><?php echo __('Actions'); ?></h3>
								<ul>
									<li><?php echo $this->Html->link(__('Edit News'), array('action' => 'edit', $news['News']['id'])); ?> </li>
									<li><?php echo $this->Form->postLink(__('Delete News'), array('action' => 'delete', $news['News']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $news['News']['id']))); ?> </li>
									<li><?php echo $this->Html->link(__('List News'), array('action' => 'index')); ?> </li>
									<li><?php echo $this->Html->link(__('New News'), array('action' => 'add')); ?> </li>
								</ul>
							</div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



