<section class="forms">
	<div class="container-fluid section-top">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header d-flex align-items-center custom-bg-color text-white">
						<h3 class="h4">Edit News</h3>
					</div>
					<div class="card-body">
						<?php echo $this->Form->create('News',array('enctype' => 'multipart/form-data')); ?>
						<?php echo $this->Form->input('id'); ?>
						<div class="form-group">
							<?php echo $this->Form->input('title',['label'=> 'News Heading','class' => 'form-control','type'=>'text']); ?>
						</div>
						<div class="form-group">
							<?php echo $this->Form->input('img',['type'=> 'file','class' => 'form-control']); ?>
						</div>
						<div class="form-group">
							<?php echo $this->Form->input('description',['class' => 'form-control','type'=>'textarea']); ?>
						</div>
						<div class="row">
							<div class="offset-md-4"></div>
							<div class="col-md-4">
								<div class="form-group">
									<button class="btn btn-primary btn-block">Update News</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
