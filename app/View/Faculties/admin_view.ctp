<div class="faculties view">
<h2><?php echo __('Faculty'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($faculty['Faculty']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('FacultyName'); ?></dt>
		<dd>
			<?php echo h($faculty['Faculty']['facultyName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DeanName'); ?></dt>
		<dd>
			<?php echo h($faculty['Faculty']['deanName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($faculty['Faculty']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($faculty['Faculty']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Faculty'), array('action' => 'edit', $faculty['Faculty']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Faculty'), array('action' => 'delete', $faculty['Faculty']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $faculty['Faculty']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Faculties'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Faculty'), array('action' => 'add')); ?> </li>
	</ul>
</div>
