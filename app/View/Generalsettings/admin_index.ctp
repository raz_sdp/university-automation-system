<section class="tables">
    <div class="container-fluid section-top">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">General Settings</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
									<th><?php echo $this->Paginator->sort('logo'); ?></th>
									<th><?php echo $this->Paginator->sort('slogan'); ?></th>
									<th><?php echo $this->Paginator->sort('companyName'); ?></th>
									<th><?php echo $this->Paginator->sort('welcomeMsg'); ?></th>
									<th><?php echo $this->Paginator->sort('principalMsg'); ?></th>
									<th><?php echo $this->Paginator->sort('location'); ?></th>
									<th><?php echo $this->Paginator->sort('telephone'); ?></th>
									<th><?php echo $this->Paginator->sort('phone'); ?></th>
									<th><?php echo $this->Paginator->sort('mail'); ?></th>
									<th><?php echo $this->Paginator->sort('created'); ?></th>
									<th><?php echo $this->Paginator->sort('modified'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($generalsettings as $generalsetting): ?>
                                    <tr>
										<td><?php echo h($generalsetting['Generalsetting']['id']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['logo']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['slogan']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['companyName']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['welcomeMsg']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['principalMsg']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['location']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['telephone']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['phone']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['mail']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['created']); ?>&nbsp;</td>
										<td><?php echo h($generalsetting['Generalsetting']['modified']); ?>&nbsp;</td>
										<td class="actions">
											<?php echo $this->Html->link(__('View'), array('action' => 'view', $generalsetting['Generalsetting']['id'])); ?>
											<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $generalsetting['Generalsetting']['id'])); ?>
											<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $generalsetting['Generalsetting']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $generalsetting['Generalsetting']['id']))); ?>
										</td>
									</tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>

                        </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center pull-right">
                                <?php
                                echo '<li class="page-item">'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'page-item disabled')).'</li>';
                                echo '<li class="page-item">'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li class="page-item">'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
