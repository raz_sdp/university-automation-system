<section class="forms">
    <div class="container-fluid section-top">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-header d-flex align-items-center custom-bg-color text-white">
                        <h3 class="h4">Add General Setting</h3>
                    </div>
                    <div class="card-body">
                        <?php echo $this->Form->create('Generalsetting',array('enctype' => 'multipart/form-data')); ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('logo',['type'=> 'file','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('slogan',['type'=> 'text','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('companyName',['label'=> 'University Name','type'=> 'text','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('welcomeMsg',['label'=> 'Welcome Message','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('principalMsg',['label'=> 'Principal Message','type'=> 'textarea','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('location',['type'=> 'text','class' => 'form-control']); ?>
                        </div>
                         <div class="form-group">
                            <?php echo $this->Form->input('telephone',['type'=> 'text','class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('phone',['type'=> 'text','class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('mail',['class' => 'form-control','type'=>'text']); ?>
                        </div>
                        <div class="row">
                            <div class="offset-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block">Add General Setting</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>