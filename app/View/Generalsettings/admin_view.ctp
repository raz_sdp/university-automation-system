<section class="tables">
    <div class="container-fluid section-top">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Notices</h3>
                    </div>
                    <div class="card-body">
                        <div class="generalsettings view p-5">
						<h2><?php echo __('Generalsetting'); ?></h2>
							<dl class = "dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['id']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Logo'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['logo']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Slogan'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['slogan']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('CompanyName'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['companyName']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('WelcomeMsg'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['welcomeMsg']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('PrincipalMsg'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['principalMsg']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Location'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['location']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Telephone'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['telephone']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Phone'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['phone']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Mail'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['mail']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Created'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['created']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Modified'); ?></dt>
								<dd>
									<?php echo h($generalsetting['Generalsetting']['modified']); ?>
									&nbsp;
								</dd>
							</dl>
						</div>
						<div class="actions">
							<h3><?php echo __('Actions'); ?></h3>
							<ul>
								<li><?php echo $this->Html->link(__('Edit Generalsetting'), array('action' => 'edit', $generalsetting['Generalsetting']['id'])); ?> </li>
								<li><?php echo $this->Form->postLink(__('Delete Generalsetting'), array('action' => 'delete', $generalsetting['Generalsetting']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $generalsetting['Generalsetting']['id']))); ?> </li>
								<li><?php echo $this->Html->link(__('List Generalsettings'), array('action' => 'index')); ?> </li>
								<li><?php echo $this->Html->link(__('New Generalsetting'), array('action' => 'add')); ?> </li>
							</ul>
						</div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
