
<section class="forms">
    <div class="container-fluid section-top">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-header d-flex align-items-center custom-bg-color text-white">
                        <h3 class="h4">Admin Add Glance</h3>
                    </div>
                    <div class="card-body">
                        <?php echo $this->Form->create('Glance',array('enctype' => 'multipart/form-data')); ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('icon',['label'=> 'Font Awesome Icon','type'=> 'text','class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('title',['label'=> 'Title','class' => 'form-control','type'=>'text']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('description',['class' => 'form-control','type'=>'text']); ?>
                        </div>
                        <div class="row">
                            <div class="offset-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block">Add Glance</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
