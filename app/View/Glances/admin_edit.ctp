
<section class="forms">
    <div class="container-fluid section-top">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-header d-flex align-items-center custom-bg-color text-white">
                        <h3 class="h4">Edit Glance</h3>
                    </div>
                    <div class="card-body">
                        <?php echo $this->Form->create('Glance',array('enctype' => 'multipart/form-data')); ?>
                        <?php echo $this->Form->input('id'); ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('icon',['label'=> 'Font Awesome Icon','type'=> 'text','class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('title',['label'=> 'Title','class' => 'form-control','type'=>'text']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('description',['class' => 'form-control','type'=>'text']); ?>
                        </div>
                        <div class="row">
                            <div class="offset-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block">Update Glance</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<div class="glances form">
<?php echo $this->Form->create('Glance'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Glance'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('icon');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Glance.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Glance.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Glances'), array('action' => 'index')); ?></li>
	</ul>
</div>
