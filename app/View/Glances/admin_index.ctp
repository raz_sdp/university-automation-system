<div class="glances index">
	<h2><?php echo __('Glances'); ?></h2>
	<table class="table table-hover" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('icon'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($glances as $glance): ?>
	<tr>
		<td><?php echo h($glance['Glance']['id']); ?>&nbsp;</td>
		<td><?php echo h($glance['Glance']['icon']); ?>&nbsp;</td>
		<td><?php echo h($glance['Glance']['title']); ?>&nbsp;</td>
		<td><?php echo h($glance['Glance']['description']); ?>&nbsp;</td>
		<td><?php echo h($glance['Glance']['created']); ?>&nbsp;</td>
		<td><?php echo h($glance['Glance']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $glance['Glance']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $glance['Glance']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $glance['Glance']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $glance['Glance']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Glance'), array('action' => 'add')); ?></li>
	</ul>
</div>
