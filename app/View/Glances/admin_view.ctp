<div class="glances view">
<h2><?php echo __('Glance'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($glance['Glance']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Icon'); ?></dt>
		<dd>
			<?php echo h($glance['Glance']['icon']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($glance['Glance']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($glance['Glance']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($glance['Glance']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($glance['Glance']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Glance'), array('action' => 'edit', $glance['Glance']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Glance'), array('action' => 'delete', $glance['Glance']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $glance['Glance']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Glances'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Glance'), array('action' => 'add')); ?> </li>
	</ul>
</div>
