<?php
$ctrl = strtolower($this->params['controller']);
$action = strtolower($this->params['action']);
$active = @($this->params[pass][0]);
?>
<nav class="side-navbar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar"><img src="<?php echo $this->Html->url('/img/avatar-1.jpg',true) ?>" alt="..." class="img-fluid rounded-circle"></div>
        <div class="title">
            <h1 class="h4">Admin</h1>
            <p>University Automation System</p>
        </div>
    </div>
    <!-- Sidebar Navidation Menus-->
    <!--    <span class="heading">Main</span>-->
    <ul class="list-unstyled">
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_dashboard' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/dashboard') ?>"> <i class="fas fa-home"></i> Home </a></li>

        <li><a href="#generalsettings" aria-expanded="<?php echo $ctrl == 'generalsettings' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-cogs"></i> General Setting <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="generalsettings" class="<?php echo $ctrl == 'generalsettings' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'generalsettings' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/generalsettings/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'generalsettings' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/generalsettings') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#sliders" aria-expanded="<?php echo $ctrl == 'sliders' ? 'true' : 'false' ?>" data-toggle="collapse"><i class="fas fa-audio-description"></i> Sliders Setting <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="sliders" class="<?php echo $ctrl == 'sliders' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'sliders' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/sliders/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'sliders' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/sliders') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#Notices" aria-expanded="<?php echo $ctrl == 'Notices' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-align-left"></i> Manage Notices <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="Notices" class="<?php echo $ctrl == 'Notices' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'Notices' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/notices/add') ?>"><i class="fas fa-plus"></i> Add New Notice</a></li>
                <li class="<?php echo $ctrl == 'Notices' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/notices') ?>"><i class="fas fa-list-ul"></i> Notices List</a></li>
            </ul>
        </li>

         <li><a href="#news" aria-expanded="<?php echo $ctrl == 'news' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-newspaper"></i> Manage News & Events <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="news" class="<?php echo $ctrl == 'news' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'news' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/news/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'news' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/news') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#facilities" aria-expanded="<?php echo $ctrl == 'facilities' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-university"></i> Manage Facilities <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="facilities" class="<?php echo $ctrl == 'facilities' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'facilities' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/facilities/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'facilities' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/facilities') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#glances" aria-expanded="<?php echo $ctrl == 'glances' ? 'true' : 'false' ?>" data-toggle="collapse"><i class="fas fa-arrow-alt-circle-right"></i> At a Glance <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="glances" class="<?php echo $ctrl == 'glances' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'glances' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/glances/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'glances' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/glances') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
         <li><a href="#faculties" aria-expanded="<?php echo $ctrl == 'faculties' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-university"></i> Manage Faculties <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="faculties" class="<?php echo $ctrl == 'faculties' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'faculties' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/faculties/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'faculties' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/faculties') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#departments" aria-expanded="<?php echo $ctrl == 'departments' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-link"></i> Manage Departments <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="departments" class="<?php echo $ctrl == 'departments' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'departments' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/departments/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'departments' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/departments') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#activities" aria-expanded="<?php echo $ctrl == 'activities' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-link"></i> Activities <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="activities" class="<?php echo $ctrl == 'activities' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'activities' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/activities/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'activities' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/activities') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#semesters" aria-expanded="<?php echo $ctrl == 'semesters' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-link"></i> Semesters <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="semesters" class="<?php echo $ctrl == 'semesters' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'semesters' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/semesters/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'semesters' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/semesters') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#syllabuses" aria-expanded="<?php echo $ctrl == 'syllabus' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-credit-card"></i> Syllabuses <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="syllabuses" class="<?php echo $ctrl == 'syllabus' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'syllabus' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/syllabi/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'syllabus' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/syllabi/index') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#Category" aria-expanded="<?php echo $ctrl == 'credits' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-credit-card"></i> Income <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="Category" class="<?php echo $ctrl == 'credits' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'credits' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/credits/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'credits' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/credits/index') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li class="open"><a href="#product" aria-expanded="<?php echo $ctrl == 'expences' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="far fa-money-bill-alt"></i> Expenses <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="product" class="<?php echo $ctrl == 'expences' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'expences' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/expences/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'expences' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/expences/index') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#invoice" aria-expanded="<?php echo $ctrl == 'employees' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-user-tie"></i> Employees <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="invoice" class="<?php echo $ctrl == 'employees' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'employees' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/employees/add') ?>"><i class="fas fa-plus"></i> Add</a></li>
                <li class="<?php echo $ctrl == 'employees' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/employees/index') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li><a href="#sale" aria-expanded="<?php echo $ctrl == 'salaries' ? 'true' : 'false' ?>" data-toggle="collapse"> <i class="fas fa-hand-holding-usd"></i> Salary <span class="float-right"><i class="fas fa-angle-left"></i></span></a>
            <ul id="sale" class="<?php echo $ctrl == 'salaries' ? '' : 'collapse' ?> list-unstyled ">
                <li class="<?php echo $ctrl == 'salaries' && $action == 'admin_add' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/salaries/add') ?>"><i class="fas fa-plus"></i> Pay</a></li>
                <li class="<?php echo $ctrl == 'salaries' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/salaries/index') ?>"><i class="fas fa-list-ul"></i> List</a></li>
            </ul>
        </li>
        <li class="<?php echo $ctrl == 'ledgers' && $action == 'admin_index' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/ledgers/index') ?>"> <i class="fas fa-balance-scale"></i> Ledger</a></li>
        <li class="<?php echo $ctrl == 'users' && $action == 'admin_reset_password' ? 'active' : '' ?>"><a href="<?php echo $this->Html->url('/admin/users/reset_password') ?>"> <i class="fas fa-key"></i> Reset Password</a></li>
        <!--        <li><a href="tables.html"> <i class="icon-grid"></i>Tables </a></li>-->
        <!--        <li><a href="charts.html"> <i class="fa fa-bar-chart"></i>Charts </a></li>-->
        <!--        <li><a href="forms.html"> <i class="icon-padnote"></i>Forms </a></li>-->
        <!---->
        <!--        <li><a href="login.html"> <i class="icon-interface-windows"></i>Login page </a></li>-->
        <!--    </ul><span class="heading">Extras</span>-->
        <!--    <ul class="list-unstyled">-->
            <!--        <li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>-->
            <!--        <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>-->
            <!--        <li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>-->
            <!--        <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>-->
        </ul>
    </nav>
    <div class="content-inner">
