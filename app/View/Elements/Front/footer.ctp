<footer class="footer pt-4 pb-4">
	<div class="container text-white">
		<div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12">
				<h4 class="font-weight-bold  pt-3 pb-3">QUICK CONTACT INFO</h4>
				<p><i class=" pr-2 fas fa-map-marker-alt"></i> <?=$gen_sett['Generalsetting']['location']?></p>
				<p><i class=" pr-2 fas fa-phone"></i> Tel: <?=$gen_sett['Generalsetting']['telephone']?></p>
				<p><i class=" pr-2 fas fa-phone"></i> Phone: <?=$gen_sett['Generalsetting']['phone']?></p>
				<p><i class=" pr-2 fas fa-envelope"></i> Email: <?=$gen_sett['Generalsetting']['mail']?></p>
			</div>
			<div class="col-lg-3 col-md-12 col-sm-12">
				<h4 class="font-weight-bold  pt-3 pb-3">QUICK LINKS</h4>
				<ul class="nav flex-column">
					<li class="nav-item"><a class="nav-link" href="">IT Department</a></li>
					<li class="nav-item"><a class="nav-link" href="">Job Openings</a></li>
					<li class="nav-item"><a class="nav-link" href="">Webmail</a></li>
					<li class="nav-item"><a class="nav-link" href="">Student TER</a></li>
					<li class="nav-item"><a class="nav-link" href="">Registration Package</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12">
				<h4 class="font-weight-bold pt-3 pb-3">IMPORTANT LINKS</h4>
				<div class="text-white">
					<a href="#" target="_blank">
						<img class="img-fluid" src="img/circular.png" alt="circular" />
					</a>
					<hr />
					<a href="#" target="_blank">
						<img class="img-fluid" src="img/result.png" alt="result" />
						<!--  --></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
    <?php
        echo $this->Html->script(
            [
                'https://code.jquery.com/jquery-3.2.1.slim.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
                '../engine1/jquery',
                'owl.carousel.min',
                '../engine1/wowslider',
                '../engine1/script',
                'https://unpkg.com/aos@2.3.1/dist/aos.js'
            ]
        );
    ?>
		<script>
        /* ========================================== 
        scrollTop() >= 200
        Should be equal the the height of the header
        ========================================== */

        $(window).scroll(function() {
            if ($(window).scrollTop() >= 200) {
                $('nav').addClass('fixed-header');
                $('nav div').addClass('visible-title');
            } else {
                $('nav').removeClass('fixed-header');
                $('nav div').removeClass('visible-title');
            }
        });
        </script>
        
        <script>
            $('.nav-tabs[data-toggle="tab-hover"] > li > a').hover(function() {
                $(this).tab('show');
            });
        </script>
        <script>
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                items: 4,
                loop: true,
                margin: 10,
                autoplay: false,
                autoplayTimeout: 2000,
                autoplayHoverPause: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    700: {
                        items: 1
                    },
                    992: {
                        items: 2
                    },
                    1170:{
                        items:3
                    }
                }
            });
        </script>
        <script>
          AOS.init({
            duration : 1500
          });
        </script>