<?php
#pr($gen_sett);
?>
<section>
	<header>
		<div class="header-banner">
			<div class="container mobile-device">
				<a href="#">
					<img src="<?php echo $this->Html->url('/files/generalsetting/'.$gen_sett['Generalsetting']['logo'])?>">
				</a>
				<span>
					<h1><?=$gen_sett['Generalsetting']['companyName']?></h1>
					<h5><?=$gen_sett['Generalsetting']['slogan']?></h5>
				</span>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-light bg-ligh">
			<div class="container">
				<div class="site-title logo">
					<a class="navbar-brand" href="#">
						<img src="<?php echo $this->Html->url('/files/generalsetting/'.$gen_sett['Generalsetting']['logo'])?>" width="60" height="50">
					</a>
				</div>
				<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"><i class="fas fa-bars navicon"></i></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class=" navbar-nav mr-auto nav-bg">
						<li class="nav-item"><a class="nav-link" href="<?php echo $this->html->url('/')?>">Home</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								About NUBT Khulna
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo $this->html->url('/about')?>">ABOUT NUB AND OUR HISTORY</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/vm')?>">VISION AND MISSION</a>
								<!-- <a class="dropdown-item" href="#">FACULTIES</a> -->
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Academic
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo $this->html->url('/ac_programs')?>">ACADEMIC PROGRAMS</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/ac_system')?>">ACADEMIC SYSTEM</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/ac_regulations')?>">ACADEMIC REGULATIONS</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/ac_calendar')?>">ACADEMIC CALENDAR</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/ac_syllabuses')?>">SYLLABUS</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Admission
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo $this->html->url('/undergraduate')?>">UNDERGRADUATE</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/postgraduate')?>">POST GRADUATE</a>
								<a class="dropdown-item" href="<?php echo $this->html->url('/diploma')?>">DIPLOMA</a>
								<a class="dropdown-item" href="<?php echo $this ->html->url('/foreign')?>">FOREIGN STUDENTS</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="#">Campus Life</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo $this->html->url('/notices-news')?>">Notices & News</a></li>
						<li class="nav-item"><a class="nav-link" href="#">library</a></li>
						<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
</section>