<div class="syllabi view table table-hover">
<h2><?php echo __('Syllabus'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($syllabus['Syllabus']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Department'); ?></dt>
		<dd>
			<?php echo $this->Html->link($syllabus['Department']['id'], array('controller' => 'departments', 'action' => 'view', $syllabus['Department']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($syllabus['Syllabus']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File'); ?></dt>
		<dd>
			<?php echo h($syllabus['Syllabus']['file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($syllabus['Syllabus']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Syllabus'), array('action' => 'edit', $syllabus['Syllabus']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Syllabus'), array('action' => 'delete', $syllabus['Syllabus']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $syllabus['Syllabus']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Syllabi'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Syllabus'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
	</ul>
</div>
