<div class="syllabi index table table-hover">
	<h2><?php echo __('Syllabi'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('department_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('file'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($syllabi as $syllabus): ?>
	<tr>
		<td><?php echo h($syllabus['Syllabus']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($syllabus['Department']['id'], array('controller' => 'departments', 'action' => 'view', $syllabus['Department']['id'])); ?>
		</td>
		<td><?php echo h($syllabus['Syllabus']['modified']); ?>&nbsp;</td>
		<td><?php echo h($syllabus['Syllabus']['file']); ?>&nbsp;</td>
		<td><?php echo h($syllabus['Syllabus']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $syllabus['Syllabus']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $syllabus['Syllabus']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $syllabus['Syllabus']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $syllabus['Syllabus']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Syllabus'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
	</ul>
</div>
