<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<section class="forms">
    <div class="container-fluid section-top">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-header d-flex align-items-center custom-bg-color text-white">
                        <h3 class="h4">Admin Add Notices</h3>
                    </div>
                    <div class="card-body">
                        <?php echo $this->Form->create('Notice',array('enctype' => 'multipart/form-data')); ?>
                        <div class="form-group">
                            <?php 
                            $notice_type = [
                                'Job Circular' => 'Job Circular',
                                'Tender Notice' => 'Tender Notice',
                                'Undergraduate Admission' => 'Undergraduate Admission',
                                'Postgraduate Admission' => 'Postgraduate Admission',
                                'Notice' => 'Notice',
                            ];
                            echo $this->Form->input('type',['label'=>'Notice Type','class' => 'form-control','options'=> $notice_type]); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('title',['label'=> 'Notice Heading','class' => 'form-control','type'=> 'text']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('filename',['label'=> 'Upload File','type'=> 'file','class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <label for="datepicker">Choose Date</label>
                            <input id="datepicker" class="form-control" >
                            <?php //echo $this->Form->input('noticeDate',['class' => 'form-control','id' => 'datepicker']); ?>
                        </div>
                        <div class="row">
                            <div class="offset-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block">Add Notice</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<script>
    $(function(){
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    });
</script>