<section class="tables">
    <div class="container-fluid section-top">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Notices</h3>
                    </div>
                    <div class="card-body">
						<div class="notices view p-5">
							<h2><?php echo __('Notice'); ?></h2>
								<dl class = "dl-horizontal ">
									<dt><?php echo __('Id'); ?></dt>
									<dd>
										<?php echo h($notice['Notice']['id']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Title'); ?></dt>
									<dd>
										<?php echo h($notice['Notice']['title']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Filename'); ?></dt>
									<dd>
										<?php echo h($notice['Notice']['filename']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('NoticeDate'); ?></dt>
									<dd>
										<?php echo h($notice['Notice']['noticeDate']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Created'); ?></dt>
									<dd>
										<?php echo h($notice['Notice']['created']); ?>
										&nbsp;
									</dd>
									<dt><?php echo __('Modified'); ?></dt>
									<dd>
										<?php echo h($notice['Notice']['modified']); ?>
										&nbsp;
									</dd>
								</dl>
							</div>
							<div class="actions">
								<h3><?php echo __('Actions'); ?></h3>
								<ul>
									<li><?php echo $this->Html->link(__('Edit Notice'), array('action' => 'edit', $notice['Notice']['id'])); ?> </li>
									<li><?php echo $this->Form->postLink(__('Delete Notice'), array('action' => 'delete', $notice['Notice']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $notice['Notice']['id']))); ?> </li>
									<li><?php echo $this->Html->link(__('List Notices'), array('action' => 'index')); ?> </li>
									<li><?php echo $this->Html->link(__('New Notice'), array('action' => 'add')); ?> </li>
								</ul>
							</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>





