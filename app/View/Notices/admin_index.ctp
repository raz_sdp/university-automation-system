<section class="tables">
    <div class="container-fluid section-top">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Notices</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('type'); ?></th>
                                        <th><?php echo $this->Paginator->sort('title'); ?></th>
                                        <th><?php echo $this->Paginator->sort('filename'); ?></th>
                                        <th><?php echo $this->Paginator->sort('noticeDate'); ?></th>                                    
                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($notices as $notice): ?>
                                        <tr>
                                            <td><?php echo h($notice['Notice']['id']); ?>&nbsp;</td>
                                            <td><?php echo h($notice['Notice']['type']); ?>&nbsp;</td>
                                            <td><?php echo h($notice['Notice']['title']); ?>&nbsp;</td>
                                            <td><?php echo h($notice['Notice']['filename']); ?>&nbsp;</td>
                                            <td><?php echo date_format(date_create($notice['Notice']['noticeDate']), 'd M Y'); ?>&nbsp;</td>
                                            <td class="actions">

                                                <?php echo $this->Html->link(__('View'), array('action' => 'view', $notice['Notice']['id'])); ?>
                                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $notice['Notice']['id'])); ?>
                                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $notice['Notice']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $notice['Notice']['id']))); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>

                        </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center pull-right">
                                <?php
                                echo '<li class="page-item">'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'page-item disabled')).'</li>';
                                echo '<li class="page-item">'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li class="page-item">'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="actions">
        <h3><?php echo __('Actions'); ?></h3>
        <ul>
            
            <li><?php echo $this->Html->link(__('New Notice'), array('action' => 'add')); ?> </li>
        </ul>
    </div>
</section>
