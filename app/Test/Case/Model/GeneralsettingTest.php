<?php
App::uses('Generalsetting', 'Model');

/**
 * Generalsetting Test Case
 */
class GeneralsettingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.generalsetting'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Generalsetting = ClassRegistry::init('Generalsetting');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Generalsetting);

		parent::tearDown();
	}

}
