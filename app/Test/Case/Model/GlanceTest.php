<?php
App::uses('Glance', 'Model');

/**
 * Glance Test Case
 */
class GlanceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.glance'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Glance = ClassRegistry::init('Glance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Glance);

		parent::tearDown();
	}

}
