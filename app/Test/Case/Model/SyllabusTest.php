<?php
App::uses('Syllabus', 'Model');

/**
 * Syllabus Test Case
 */
class SyllabusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.syllabus',
		'app.department',
		'app.faculty'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Syllabus = ClassRegistry::init('Syllabus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Syllabus);

		parent::tearDown();
	}

}
