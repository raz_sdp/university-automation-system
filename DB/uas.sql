-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2019 at 12:42 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `created`, `modified`, `semester_id`, `title`, `duration`) VALUES
(1, '2019-04-02 00:21:16', '2019-04-02 00:21:16', 1, 'Classes', '1 January-15 April'),
(2, '2019-04-02 00:21:55', '2019-04-02 00:21:55', 1, 'Examination and break.', '16 April-30 April'),
(3, '2019-04-02 00:22:10', '2019-04-02 00:22:10', 2, 'Classes', '1 May-15 August'),
(4, '2019-04-02 00:22:23', '2019-04-02 00:22:23', 2, 'Examination and break.', '16 August -31 August'),
(5, '2019-04-02 00:22:48', '2019-04-02 00:22:48', 3, 'Classes', '1 September-15 December'),
(6, '2019-04-02 00:22:56', '2019-04-02 00:22:56', 3, 'Examination and break.', '16 December-31 December');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `senderName` varchar(0) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `faculty_id` int(11) DEFAULT NULL,
  `departmentName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `faculty_id`, `departmentName`, `created`, `modified`) VALUES
(2, 2, 'Computer Science and Engineering', '2019-03-21 11:05:42', '2019-03-21 11:05:42'),
(3, 2, 'Petroleum and Mining Engineering', '2019-03-21 11:06:19', '2019-03-21 11:06:19'),
(4, 2, 'Chemical Engineering', '2019-03-21 11:06:28', '2019-03-21 11:06:28'),
(5, 2, 'Electrical and Electronic Engineering', '2019-03-21 11:06:37', '2019-03-21 11:06:37'),
(6, 2, 'Industrial and Production Engineering', '2019-03-21 11:06:48', '2019-03-21 11:06:48'),
(7, 2, 'Biomedical Engineering', '2019-03-21 11:06:59', '2019-03-21 11:06:59'),
(8, 3, 'Fisheries and Marine Bioscience', '2019-03-21 11:08:17', '2019-03-21 11:08:17'),
(9, 3, 'Pharmacy', '2019-03-21 11:08:29', '2019-03-21 11:08:29'),
(10, 3, 'Genetic Engineering and Biotechnology', '2019-03-21 11:08:44', '2019-03-21 11:08:44'),
(11, 2, 'Microbiology', '2019-03-21 11:08:57', '2019-03-21 11:08:57'),
(12, 4, 'Management', '2019-03-21 11:09:21', '2019-03-21 11:09:21'),
(13, 4, 'Finance and Banking', '2019-03-21 11:09:34', '2019-03-21 11:09:34'),
(14, 4, 'Marketing', '2019-03-21 11:09:47', '2019-03-21 11:09:47'),
(15, 5, 'Bangla', '2019-03-21 11:11:59', '2019-03-21 11:11:59'),
(16, 5, 'English', '2019-03-21 11:12:10', '2019-03-21 11:12:10'),
(17, 6, 'Physical Education and Sports Science', '2019-03-21 11:12:27', '2019-03-21 11:12:27'),
(18, 7, 'Physics', '2019-03-21 11:12:51', '2019-03-21 11:12:51'),
(19, 7, 'Chemistry', '2019-03-21 11:13:00', '2019-03-21 11:13:00'),
(20, 7, ' Mathematics ', '2019-03-21 11:13:12', '2019-03-21 11:13:12'),
(21, 8, 'Environmental Science and Technology', '2019-03-21 11:13:42', '2019-03-21 11:13:42'),
(22, 8, 'Nutrition and Food Technology', '2019-03-21 11:13:54', '2019-03-21 11:13:54'),
(23, 8, 'Agro Product Processing Technology', '2019-03-21 11:14:16', '2019-03-21 11:14:16');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(11) NOT NULL,
  `medicalDesc` text COLLATE utf8_unicode_ci,
  `cafeDesc` text COLLATE utf8_unicode_ci,
  `auditoriamDesc` text COLLATE utf8_unicode_ci,
  `transportDesc` text COLLATE utf8_unicode_ci,
  `wifiDesc` text COLLATE utf8_unicode_ci,
  `libraryDesc` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `medicalDesc`, `cafeDesc`, `auditoriamDesc`, `transportDesc`, `wifiDesc`, `libraryDesc`, `created`, `modified`) VALUES
(1, 'The university has a great transportation system for the teachers, students and stuff within the major places of Jashore city. There are two coasters for teachers, three single decker buses for stuffs as well as four single decker and six double ', 'The university has a great transportation system for the teachers, students and stuff within the major places of Jashore city. There are two coasters for teachers, three single decker buses for stuffs as well as four single deckers and six double ', 'The university has a great transportation system for the teachers, students and stuffs within the major places of Jashore city. There are two coasters for teachers, three single decker buses for stuffs as well as four single decker and six double ', 'The university has a great transportation system for the teachers, students and stuffs within the major places of Jashore city. There are two coasters for teachers, three single decker buses for stuffs as well as four single decker and six double ', 'The university has a great transportation system for the teachers, students and stuffs within the major places of Jashore city. There are two coasters for teachers, three single decker buses for stuffs as well as four single decker and six double ', 'The university has a great transportation system for the teachers, students and stuffs within the major places of Jashore city. There are two coasters for teachers, three single decker buses for stuffs as well as four single decker and six double ', '2019-03-21 07:04:56', '2019-03-21 07:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `id` int(11) NOT NULL,
  `facultyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deanName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `facultyImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`id`, `facultyName`, `deanName`, `created`, `modified`, `facultyImage`) VALUES
(2, 'Faculty Of Engineering And Technology', 'DR. A.S.M. MOJAHIDUL', '2019-03-21 05:08:53', '2019-03-25 05:44:29', '1553489069457.jpg'),
(3, 'Faculty of Biological Science and Technology', 'DR. KISHOR MAZUMDER', '2019-03-21 08:38:57', '2019-03-25 05:45:20', '155348912097.jpg'),
(4, 'Faculty of Business Studies', 'PROF. DR. MD. ZIAUL AMIN', '2019-03-21 08:39:26', '2019-03-25 05:45:31', '1553489131880.jpg'),
(5, 'Faculty of Arts and Social Science', 'PROF. DR. SHAIKH MIZANUR RAHMAN', '2019-03-21 08:39:47', '2019-03-25 05:45:41', '1553489141281.jpg'),
(6, 'Faculty of Health Science', 'DR. MD. ZAFIROUL ISLAM', '2019-03-21 08:40:07', '2019-03-25 05:45:49', '1553489149966.jpg'),
(7, 'Faculty of Science', 'PROF. DR. MD. ANISUR RAHMAN', '2019-03-21 08:40:26', '2019-03-25 05:45:59', '1553489159476.jpg'),
(8, 'Faculty of Applied Science and Technology', 'DR. MD. OMAR FARUQUE', '2019-03-21 08:40:53', '2019-03-25 05:46:07', '1553489167278.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `generalsettings`
--

CREATE TABLE `generalsettings` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slogan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `welcomeMsg` longtext COLLATE utf8_unicode_ci,
  `principalMsg` longtext COLLATE utf8_unicode_ci,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `generalsettings`
--

INSERT INTO `generalsettings` (`id`, `logo`, `slogan`, `companyName`, `welcomeMsg`, `principalMsg`, `location`, `telephone`, `phone`, `mail`, `created`, `modified`) VALUES
(2, '1553059565768.png', 'way to go , out of the box', 'Khulna University of Engineering & Technology', '    <p class=\"p-2\">Prof. Dr. Abu YousufMd Abdullah was born in Satkhira and studied at Jhenidah Cadet College. He\r\n        completed his graduation from Dhaka University and MBA from the Institute of Business Administration (IBA) under\r\n        the University of Dhaka (DU). He obtained his INTERNATIOAL MBA from Helsinki School of Economics and Business\r\n        Administration, Finland. He perused his International MBA also from the Graduate Business School of the\r\n        University of Texas at Austin, Texas, U.S.A. After returning from the U.S.A, he joined IBA as a Lecturer and\r\n        obtained his PhD from the University of Dhaka. He has been actively involved in teaching for more than 22 years\r\n        at IBA.\r\n    </p>\r\n\r\n    <p>\r\n        <strong class=\"pt-1\">A Message From The Chairman\r\n        </strong>\r\n    </p>\r\n\r\n    <p>It is with great pride and immense pleasure that I watch Northern University Bangladesh as it treads to attain\r\n        the highest ranking status with its center of excellence in the field of higher education in Bangladesh. The 17\r\n        October 2002 marked the beginning of a trailblazing journey for this prestigious institution along with a group\r\n        of highly qualified and illustrious educationists of the country. Since then, these educationists have been\r\n        tirelessly working towards one goal- to provide higher education to the citizens of the nation that is at par\r\n        with globally accepted standards in a dynamic academic landscape\r\n    </p>\r\n\r\n    <p>When I had taken on the role as Chairperson, Northern University Bangladesh Trust, I felt incredibly humbled and\r\n        inspired to see that an institution can consistently produce a service/product that is not only deemed to be of\r\n        premium quality in the higher education arena and cater to only some of our brightest minds, yet also keep\r\n        higher education within the reach for students who come from modest economic backgrounds. To do so is not an\r\n        easy task, as it requires a constant focus on the rapid changes that is resultant from the combined forces of\r\n        tremendous technological advancement and free enterprise. Everything from radical innovations in communications,\r\n        the explosion of the WORLD WIDE WEB, the opening of several new avenues for commerce such as Event management,\r\n        Activation, Direct marketing, Business Process Outsourcing and many more, has completely changed the face of the\r\n        global economy. This has resulted in us finally being classified as world citizens. Taking all these elements\r\n        into account, I commend the Vice Chancellor and our team of highly qualified staff & teachers, for effectively\r\n        keeping in pace with these changes. Needless to say, without their efforts to keep up with the rest of the\r\n        world, we as a nation would seriously fall behind.\r\n    </p>\r\n\r\n    <p>At Northern University Bangladesh, we aim to provide our student with an education that is absolutely relevant to\r\n        todayâ€™s world and constantly update our curricula to incorporate them. We believe in providing higher education\r\n        that will not only equip the student with all the necessary tools and skills to excel in their chosen fields but\r\n        that which can also facilitate innovation leading towards socio-economic empowerment, which will subsequently\r\n        build a brighter future for Bangladesh as a nation.\r\n    </p>\r\n\r\n    <p>To the future students of Northern University Bangladesh, make use of this time at your university to explore\r\n        your talents, seize the day everyday at NUB and absorb all the knowledge you may receive from our capable\r\n        teachers. This is a crucial time in your lives where you decide how you plan to navigate your journey in life\r\n        and who you wish to be. I would like to implore our younger generation to keep in mind that in their future\r\n        lives and careers, it is imperative to remember that they have a duty towards their country and that they must\r\n        incorporate social responsibility into their future endeavors. This will not only benefit them as individuals by\r\n        building character, but will benefit society as a whole.\r\n    </p>\r\n\r\n    <p>\r\n        <strong>Prof. Abu Yousuf Md. Abdullah PhD\r\n        </strong>\r\n        <br>\r\n        Chairman, NUB TRUST.\r\n        <br>\r\n        The Governing Body of,\r\n        <br>\r\n        Northern University Bangladesh.\r\n    </p>', 'As you are aware of the fact that the vision of Northern University Bangladesh(NUB) is to create knowledge for the socio-economic development of the people of the region and their overall empowerment through application oriented disciplines to raise their skill; all our efforts in NUB is focused to those goals and our strategies are geared to that. At NUB we seek through a seamless effort to nurture and develop the young as well as matured minds with a mindset that constantly innovates and creates new ideas in all the braches of higher education.\r\n\r\nOur pedagogy is modern as well as student friendly, our class room facilities are of international standard and our faculty members are world class. In this world of cut throat competition and cutting edge technology we have taken the challenge to groom our students with the skill which they can use from the day one as they enter into the job market or build their own enterprise.\r\n\r\nIn line with the goal of the university to create, disseminate and store new knowledge, we emphasize on teaching, undertaking research and regular publication in our university. All our campuses are well equipped with technology driven teaching aids, and all our scientific labs are providing excellent research opportunities both for students and the faculty members. Equal research facilities are also available for the business school and social science students and faculty members. We also regularly publish journals from all the faculties of the universities which are blind refereed. We patronize with great care case research for the business and the law students.\r\n\r\nWe are compliant to government and UGC rules and our permanent campus is now being built with all the facilities of a modern campus and we hope to move there within a foreseeable future. It may be mentioned here that part of our permanent campus is already operational with some of the departments of science faculty holding their classes regularly in that campus.\r\n\r\nThe economy of Bangladesh is growing steadily and the private sector is now hyper active due to supportive government policies . We now require regular flow of young and skilled graduates to run the enterprises coming up in the country. NUB is working relentlessly to provide this service for our economy.\r\n\r\nWe believe on the basic values of the society and the spirit of our glorious liberation war and remember with gratitude the sacrifices of our martyrs and the valiant freedom struggle of our people under the legendary leadership of our father of the nation Bangabandhu Shaikh Mujibur Rahman. We want our graduates to reflect these values and be the standard bearer of their almamater in whatever capacity they are serving in future.\r\n\r\nProf. Dr. Anwar Hossain \r\nVice-Chancellor \r\nNorthern University Bangladesh', 'Sher Tower, Holding #13, Road #17, Banani C/A, Dhaka', '01924161357', '01924161357', 'info@nub.ac.bd', '2019-03-20 06:26:05', '2019-03-21 11:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `glance`
--

CREATE TABLE `glance` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `glance`
--

INSERT INTO `glance` (`id`, `icon`, `title`, `description`, `created`, `modified`) VALUES
(1, '<i class=\"fas fa-home size7x\"></i>', '420', 'Lorem Ipsum', '2019-03-21 08:09:46', '2019-03-21 08:09:46'),
(2, '<i class=\"fas fa-ambulance size7x\"></i>', '420', 'Lorem Ipsum', '2019-03-21 08:10:03', '2019-03-21 08:10:03'),
(3, '<i class=\"fab fa-accessible-icon size7x\"></i>', '420', 'Lorem Ipsum', '2019-03-21 08:10:18', '2019-03-21 08:10:18'),
(4, '<i class=\"fas fa-american-sign-language-interpreting size7x\"></i>', '420', 'Lorem Ipsum', '2019-03-21 08:10:32', '2019-03-21 08:10:32'),
(5, '<i class=\"fab fa-apple-pay size7x\"></i>', '420', 'Lorem Ipsum', '2019-03-21 08:10:45', '2019-03-21 08:10:45'),
(6, '<i class=\"fab fa-accusoft size7x\"></i>', '420', 'Lorem Ipsum', '2019-03-21 08:10:57', '2019-03-21 08:10:57');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `img`, `title`, `description`, `created`, `modified`) VALUES
(1, '1553505307329.jpg', '32nd Bangladesh Society of Microbiology Annual Conference (BSM-2019)', 'njdnkjvndskjvjkd jkd dkj kjn  knk  ubsvv   knf nar  erknaef vnvl', '2019-03-19 07:53:44', '2019-03-25 10:15:08'),
(2, '1553505381188.jpg', 'à¦¯à¦¬à¦¿à¦ªà§à¦°à¦¬à¦¿à¦¤à§‡ à¦¸à§à¦¬à¦¾à¦§à§€à¦¨à¦¤à¦¾ à¦¦à¦¿à¦¬à¦¸ à¦†à¦¨à§à¦¤à¦ƒà¦¬à¦¿à¦¶à§à¦¬à¦¬à¦¿à¦¦à§à¦¯à¦¾à¦²à§Ÿ à¦¬à¦¿à¦¤à¦°à§à¦• à¦‰à§Žà¦¸à¦¬ à¦¯à¦¬à¦¿à¦ªà§à¦°à¦¬à¦¿à¦¤à§‡ à¦ à¦¬à¦›à¦°à¦‡ à¦›à¦¾à¦¤à§à¦° à¦¸à¦‚à¦¸à¦¦ à¦¨à¦¿à¦°à§à¦¬à¦¾à¦šà¦¨: à¦…à¦§à§à¦¯à¦¾à¦ªà¦• à¦†à¦¨à§‹à§Ÿà¦¾à¦°', 'fgffbgfbf gbfb', '2019-03-19 09:38:02', '2019-03-25 10:16:21'),
(3, '1553505416613.jpg', 'à¦¯à¦¬à¦¿à¦ªà§à¦°à¦¬à¦¿à¦¤à§‡ à¦¦à¦¿à¦¨à¦¬à§à¦¯à¦¾à¦ªà§€ à¦œà¦®à¦œà¦®à¦¾à¦Ÿ à¦¬à¦¿à¦œà§à¦žà¦¾à¦¨ à¦®à§‡à¦²à¦¾', 'sjkdals', '2019-03-19 10:45:00', '2019-03-25 10:16:56'),
(4, '1552988773334.jpg', 'hghg', 'dsadsadsa', '2019-03-19 10:46:14', '2019-03-19 10:46:14'),
(5, '1552990068317.jpg', 'Test new & event', 'njdnkjvndskjvjkd jkd dkj kjn  knk  ubsvv   knf nar  erknaef vnvl', '2019-03-19 11:07:49', '2019-03-19 11:07:49'),
(6, '1552990154590.jpg', 'hghg', 'sjkdals', '2019-03-19 11:09:14', '2019-03-19 11:09:14');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noticeDate` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `type` enum('Job Circular','Tender Notice','Postgraduate Admission','Undergraduate Admission','Notice') COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `title`, `filename`, `noticeDate`, `created`, `modified`, `type`) VALUES
(5, 'à§§à¦® à¦¬à¦¾à¦°à§à¦·à¦¿à¦• à¦•à§à¦°à§€à§œà¦¾ à¦ªà§à¦°à¦¤à¦¿à¦¯à§‹à¦—à§€à¦¤à¦¾-à§¨à§¦à§§à§¯ (à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿ à¦“ à¦à¦¨à§à¦Ÿà§à¦°à¦¿ à¦«à¦°à§à¦®)', '1552985247981.pdf', '2019-03-25', '2019-03-19 09:47:27', '2019-03-25 10:52:13', 'Notice'),
(6, 'à¦ªà§‡à¦Ÿà§à¦°à§‹à¦²à¦¿à§Ÿà¦¾à¦® à¦à¦¨à§à¦¡ à¦®à¦¾à¦‡à¦¨à¦¿à¦‚ à¦‡à¦žà§à¦œà¦¿à¦¨à¦¿à§Ÿà¦¾à¦°à¦¿à¦‚ à¦¬à¦¿à¦­à¦¾à¦—à§‡à¦° à§¨à§¦à§§à§®-à§¨à§¦à§§à§¯ à¦¶à¦¿à¦•à§à¦·à¦¾à¦¬à¦°à§à¦·à§‡ à¦¸à§à¦¨à¦¾à¦¤à¦•à§‹à¦¤à§à¦¤à¦° à¦¶à§à¦°à§‡à¦£à§€à¦¤à§‡ (M.Sc. Engg.) à¦­à¦°à§à¦¤à¦¿à¦¯à§‹à¦—à§à¦¯ à¦¶à¦¿à¦•à§à¦·à¦¾à¦°à§à¦¥à§€à¦—à¦£à§‡à¦° à¦¨à¦¾à¦®à§‡à¦° à¦¤à¦¾à¦²à¦¿à¦•à¦¾', '1552985277487.pdf', '2019-03-25', '2019-03-19 09:47:57', '2019-03-25 10:53:16', 'Notice'),
(7, 'Grant of study leave for PhD Degree with pay to Ripon Kumar Adhikary, Assistant Professor, Department of FMB', '1552985303929.pdf', '2019-03-25', '2019-03-19 09:48:23', '2019-03-25 10:53:22', 'Notice'),
(8, 'à¦•à¦®à§à¦ªà¦¿à¦‰à¦Ÿà¦¾à¦° à¦¬à¦¿à¦œà§à¦žà¦¾à¦¨ à¦“ à¦ªà§à¦°à¦•à§Œà¦¶à¦² à¦¬à¦¿à¦­à¦¾à¦—à§‡à¦° à§¨à§¦à§§à§­-à§¨à§¦à§§à§® à¦¸à§‡à¦¶à¦¨à§‡à¦° M.Sc Engg./ M.Engg. à¦•à§‹à¦°à§à¦¸à§‡ à¦­à¦°à§à¦¤à¦¿à¦•à§ƒà¦¤ à¦¶à¦¿à¦•à§à¦·à¦¾à¦°à§à¦¥à§€à¦¦à§‡à¦° à¦¨à¦¾à¦®à§‡à¦° à¦¤à¦¾à¦²à¦¿à¦•à¦¾', '1552985330883.pdf', '2019-03-25', '2019-03-19 09:48:50', '2019-03-25 10:53:28', 'Notice'),
(9, 'à§§à¦® à¦¬à¦¾à¦°à§à¦·à¦¿à¦• à¦•à§à¦°à§€à§œà¦¾ à¦ªà§à¦°à¦¤à¦¿à¦¯à§‹à¦—à§€à¦¤à¦¾-à§¨à§¦à§§à§¯ (à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿ à¦“ à¦à¦¨à§à¦Ÿà§à¦°à¦¿ à¦«à¦°à§à¦®)', '1552985385376.pdf', '2019-03-25', '2019-03-19 09:49:45', '2019-03-25 10:53:33', 'Notice'),
(10, 'à¦ªà§‡à¦Ÿà§à¦°à§‹à¦²à¦¿à§Ÿà¦¾à¦® à¦à¦¨à§à¦¡ à¦®à¦¾à¦‡à¦¨à¦¿à¦‚ à¦‡à¦žà§à¦œà¦¿à¦¨à¦¿à§Ÿà¦¾à¦°à¦¿à¦‚ à¦¬à¦¿à¦­à¦¾à¦—à§‡à¦° à§¨à§¦à§§à§®-à§¨à§¦à§§à§¯ à¦¶à¦¿à¦•à§à¦·à¦¾à¦¬à¦°à§à¦·à§‡ à¦¸à§à¦¨à¦¾à¦¤à¦•à§‹à¦¤à§à¦¤à¦° à¦¶à§à¦°à§‡à¦£à§€à¦¤à§‡ (M.Sc. Engg.) à¦­à¦°à§à¦¤à¦¿à¦¯à§‹à¦—à§à¦¯ à¦¶à¦¿à¦•à§à¦·à¦¾à¦°à§à¦¥à§€à¦—à¦£à§‡à¦° à¦¨à¦¾à¦®à§‡à¦° à¦¤à¦¾à¦²à¦¿à¦•à¦¾', '1552985410706.pdf', '2019-03-25', '2019-03-19 09:50:10', '2019-03-25 10:53:41', 'Notice'),
(11, 'à§§à¦® à¦¬à¦¾à¦°à§à¦·à¦¿à¦• à¦•à§à¦°à§€à§œà¦¾ à¦ªà§à¦°à¦¤à¦¿à¦¯à§‹à¦—à§€à¦¤à¦¾-à§¨à§¦à§§à§¯ (à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿ à¦“ à¦à¦¨à§à¦Ÿà§à¦°à¦¿ à¦«à¦°à§à¦®)', NULL, '2019-03-25', '2019-03-19 10:59:22', '2019-03-25 10:53:50', 'Notice'),
(12, 'à§§à¦® à¦¬à¦¾à¦°à§à¦·à¦¿à¦• à¦•à§à¦°à§€à§œà¦¾ à¦ªà§à¦°à¦¤à¦¿à¦¯à§‹à¦—à§€à¦¤à¦¾-à§¨à§¦à§§à§¯ (à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿ à¦“ à¦à¦¨à§à¦Ÿà§à¦°à¦¿ à¦«à¦°à§à¦®)', NULL, '2019-03-25', '2019-03-19 11:01:02', '2019-03-25 10:53:56', 'Notice'),
(13, 'à§§à¦® à¦¬à¦¾à¦°à§à¦·à¦¿à¦• à¦•à§à¦°à§€à§œà¦¾ à¦ªà§à¦°à¦¤à¦¿à¦¯à§‹à¦—à§€à¦¤à¦¾-à§¨à§¦à§§à§¯ (à¦¬à¦¿à¦œà§à¦žà¦ªà§à¦¤à¦¿ à¦“ à¦à¦¨à§à¦Ÿà§à¦°à¦¿ à¦«à¦°à§à¦®)', '1553507419814.jpg', '2019-03-25', '2019-03-25 10:50:19', '2019-03-25 10:50:53', 'Notice'),
(14, 'Process of Personal Account Update (Only for Teachers, JUST)', '155350784663.jpg', '2019-03-25', '2019-03-25 10:57:26', '2019-03-25 10:57:39', 'Job Circular'),
(15, 'Grant duty leave for Md. Mahmudunnabi, Section Officer, Grade-1, Office of the registrar', '1553508023958.jpg', '2019-03-25', '2019-03-25 11:00:23', '2019-03-25 11:00:23', 'Job Circular'),
(16, 'à¦¨à§‹à¦Ÿà¦¿à¦¶-à§§ (à¦œà¦¾à¦¤à¦¿à¦° à¦œà¦¨à¦• à¦¬à¦™à§à¦—à¦¬à¦¨à§à¦§à§ à¦¶à§‡à¦– à¦®à§à¦œà¦¿à¦¬à§à¦° à¦°à¦¹à¦®à¦¾à¦¨à§‡à¦° à§¯à§¯à¦¤à¦® à¦œà¦¨à§à¦®à¦¬à¦¾à¦°à§à¦·à¦¿à¦•à§€ à¦“ à¦œà¦¾à¦¤à§€à§Ÿ à¦¶à¦¿à¦¶à§ à¦¦à¦¿à¦¬à¦¸-à§¨à§¦à§§à§¯ à¦‰à¦ªà¦²à¦•à§à¦·à§‡ à¦…à¦¨à§à¦·à§à¦ à¦¾à¦¨à¦¸à§‚à¦šà¦¿', '1553508082965.jpg', '2019-03-25', '2019-03-25 11:01:22', '2019-03-25 11:01:22', 'Job Circular'),
(17, 'Department wise List for C Unit Quota ( Admission 2018-2019 )', '1553508110911.jpg', '2019-03-25', '2019-03-25 11:01:50', '2019-03-25 11:01:50', 'Job Circular'),
(18, 'Admission Notice -02 , A Unit (Session 2018-2019)', '1553508312562.jpg', '2019-03-25', '2019-03-25 11:05:12', '2019-03-25 11:05:12', 'Job Circular'),
(19, 'e-Tender Notice for Procurement of Desktop,Laptop and related equipment.', '1553670240502.pdf', '2019-03-27', '2019-03-27 06:19:09', '2019-03-27 08:04:00', 'Tender Notice'),
(20, 'Department wise List for C Unit Quota ( Admission 2018-2019 )', '1553668317832.pdf', '2019-03-27', '2019-03-27 07:31:57', '2019-03-27 07:37:01', 'Undergraduate Admission');

-- --------------------------------------------------------

--
-- Table structure for table `semesters`
--

CREATE TABLE `semesters` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `semesters`
--

INSERT INTO `semesters` (`id`, `title`, `created`, `modified`) VALUES
(1, 'Spring (Semester I)', '2019-04-02 00:20:27', '2019-04-02 00:20:27'),
(2, 'Summer (Semester II)', '2019-04-02 00:20:31', '2019-04-02 00:20:31'),
(3, 'Fall (Semester III)', '2019-04-02 00:20:36', '2019-04-02 00:20:36');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `img`, `title`, `thumb`, `created`, `modified`) VALUES
(1, '1553080024885.jpg', 'What is Lorem Ipsum?', NULL, '2019-03-20 11:55:53', '2019-03-20 12:07:04'),
(2, '155308004950.jpg', 'Why do we use it?', NULL, '2019-03-20 12:07:29', '2019-03-20 12:07:29'),
(3, '155308006693.jpg', 'Where does it come from?', NULL, '2019-03-20 12:07:46', '2019-03-20 12:07:46'),
(4, '1553080081651.jpg', 'Where can I get some?', NULL, '2019-03-20 12:08:02', '2019-03-20 12:08:02'),
(5, '1553080098117.jpg', 'Lorem Ipsum is simply dummy text', NULL, '2019-03-20 12:08:18', '2019-03-20 12:08:18'),
(6, '1553080151760.jpg', 'Where does it come from?', NULL, '2019-03-20 12:09:11', '2019-03-20 12:09:11'),
(7, '155308016722.jpg', 'In a free hour, when our power of choice', NULL, '2019-03-20 12:09:27', '2019-03-20 12:09:27');

-- --------------------------------------------------------

--
-- Table structure for table `syllabuses`
--

CREATE TABLE `syllabuses` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `generalsettings`
--
ALTER TABLE `generalsettings`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `glance`
--
ALTER TABLE `glance`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `syllabuses`
--
ALTER TABLE `syllabuses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `generalsettings`
--
ALTER TABLE `generalsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `glance`
--
ALTER TABLE `glance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `syllabuses`
--
ALTER TABLE `syllabuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
